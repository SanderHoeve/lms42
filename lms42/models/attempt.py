from ..app import db, app
import datetime
import sqlalchemy
import json
import enum
import os
import flask
from .. import utils, working_days, email
from .user import User, AbsentDay


FORMATIVE_ACTIONS = {
    "passed": "The student passes.",
    "repair": "The student needs to correct some work.",
    "failed": "The student needs to start anew.",
}


class Attempt(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    __table_args__ = (
        db.Index('attempt_status_index', 'status', 'student_id'),
        db.Index('student_id', 'start_time'),
        db.UniqueConstraint('student_id', 'node_id', 'number'),
    )

    student_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    student = db.relationship("User", foreign_keys="Attempt.student_id", back_populates="attempts")

    number = db.Column(db.Integer, nullable=False)

    node_id = db.Column(db.String, nullable=False)
    variant_id = db.Column(db.SmallInteger, nullable=False)

    start_time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    deadline_time = db.Column(db.DateTime)
    upload_time = db.Column(db.DateTime)
    submit_time = db.Column(db.DateTime)


    status = db.Column(db.String, nullable=False) # awaiting_approval, in_progress, needs_grading, needs_consent, passed, repair, failed
    finished = db.Column(db.String) # yes, deadline, forfeit, accidental

    credits = db.Column(db.Integer, nullable=False, default=0)
    
    avg_days = db.Column(db.Float, nullable=False)

    alternatives = db.Column(db.JSON, default=dict(), nullable=False)

    total_hours = db.Column(db.Numeric(10,2))

    @property
    def hours(self):
        """Returns the number of hours spent on the assignment, excluding structural absent days and regular
        off-time (such as evenings, weekends and vacations).
        We assume that when submit_time is set, the `total_hours` property will be set to our return value.
        As our return value shouldn't change from then on, we will just return `total_hours`.
        """
        if self.total_hours != None:
            return self.total_hours
        leave_days = {ad.date for ad in AbsentDay.query.filter_by(user_id=self.student_id, reason='absent_structural')}
        return working_days.get_working_hours_delta(self.start_time, self.submit_time or datetime.datetime.utcnow(), leave_days)

    @property
    def latest_grading(self):
        return self.gradings.order_by(Grading.id.desc()).first()

    @property
    def directory(self):
        return os.path.join(app.config['DATA_DIR'], 'attempts', f"{self.node_id}-{self.student_id}-{self.number}")

    @property
    def node_name(self):
        nodes = curriculum.get('nodes_by_id')
        if self.node_id not in nodes:
            return self.node_id
        node = nodes[self.node_id]
        name = node["name"]
        topic = curriculum.get('modules_by_id')[node["module_id"]]
        if topic and topic.get("name"):
            name = topic.get("short", topic.get("name")) + " ➤ " + name
        return name

    @property
    def inbox_date(self):
        time = self.submit_time or self.deadline_time
        return time.date()

    def write_json(self):
        d = utils.model_to_dict(self)
        d["student"] = {
            "full_name": self.student.full_name,
            "email": self.student.email,
        }
        d["gradings"] = [utils.model_to_dict(grading) for grading in self.gradings]
        with open(os.path.join(self.directory, "attempt.json"), "w") as file:
            file.write(json.dumps(d,indent=4))


def get_notifications(attempt, grading):
    notifications = []

    if attempt.status == "in_progress":
        notifications.append("In progress!")

    if attempt.submit_time:
        notifications.append(f"Attempt by {attempt.student.full_name} from {utils.utc_to_display(attempt.start_time)} until {utils.utc_to_display(attempt.submit_time)}.")
    else:
        notifications.append(f"Attempt by {attempt.student.full_name} started {utils.utc_to_display(attempt.start_time)}.")

    if attempt.deadline_time:
        if attempt.status == "in_progress":
            notifications.append(f"Deadline: {utils.utc_to_display(attempt.deadline_time)}.")

        exceed = working_days.get_working_hours_delta(attempt.deadline_time, attempt.submit_time or datetime.datetime.utcnow())
        if exceed > 0:
            notifications.append(f"<strong>Deadline exceeded</strong> by {round(exceed,1)} hours!")

    if attempt.finished in ["forfeit", "deadline", "accidental"]:
        notifications.append(f"Attempt not finished: <strong>{attempt.finished}</strong>.")

    if grading:
        notifications.append(f"Graded by {grading.grader.full_name} on {utils.utc_to_display(grading.time)}.")
        if grading.needs_consent:
            notifications.append(f"Awaiting consent by a second examinator.")
        else:
            if grading.consent_user_id:
                notifications.append(f"Consent given by {grading.consenter.full_name} on {utils.utc_to_display(grading.consent_time)}.")
            if grading.grade_motivation:
                notifications.append(f"Teacher feedback:<div class='notification is-info'>{utils.markdown_to_html(grading.grade_motivation)}</div>")
            if attempt.status in FORMATIVE_ACTIONS:
                notifications.append(f"<strong>{FORMATIVE_ACTIONS[attempt.status]}</strong>")

    return notifications



from . import curriculum
from .grading import Grading
