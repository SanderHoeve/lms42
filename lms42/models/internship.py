from ..app import app, db


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String, nullable=False)
    logo = db.Column(db.String)
    size = db.Column(db.String)
    location = db.Column(db.String)
    description = db.Column(db.String)
    promotext = db.Column(db.String, nullable=False)
    intern_fee = db.Column(db.Integer, nullable=False)
    misc = db.Column(db.String)
    
    # Loading related teams via a LEFT OUTER JOIN (lazy='joined') each
    # time a Company is loaded, solving the N+1 problem.
    teams = db.relationship(
        'Team', 
        backref='company', 
        lazy='joined',
        cascade="all, delete-orphan",
        )

    @property
    def logo_file(self):
        if self.logo:
            return f"/static/logos/{self.logo}" 


class Team(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String, nullable=False)
    availability = db.Column(db.Integer, nullable=False, default=0)
    size = db.Column(db.String, nullable=False)
    location = db.Column(db.String, nullable=False)
    technologies = db.Column(db.String, nullable=False)
    working_on = db.Column(db.String)
    misc = db.Column(db.String)
    contact = db.Column(db.String, nullable=False)
    dutch_only = db.Column(db.Boolean, default=False)
    
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'))