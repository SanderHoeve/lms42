from ..app import db, app
import datetime
import sqlalchemy
import json
import enum
import os
import requests
import urllib
from .grading import Grading
from .user import User


class NodeFeedback(db.Model):
    node_id = db.Column(db.String, nullable=False)

    student_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    student = db.relationship("User", foreign_keys="NodeFeedback.student_id")

    __table_args__ = (
        db.PrimaryKeyConstraint(node_id,student_id),
    )

    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)

    assignment_clarity = db.Column(db.SmallInteger) # 1-5 (5 is optimal)
    difficulty = db.Column(db.SmallInteger) # 1-5 (3 is optimal)
    hours = db.Column(db.SmallInteger) # in hours
    fun = db.Column(db.SmallInteger) # 1-5 (5 is optimal)
    resource_quality = db.Column(db.SmallInteger) # 1-5 (5 is optimal)

    comments = db.Column(db.Text)

    def post_to_gitlab(self):
        comments = (self.comments or '').strip()
        if not comments:
            return

        base_url = os.environ.get("GITLAB_FEEDBACK_INSTANCE") or "https://gitlab.com"
        project = os.environ.get("GITLAB_FEEDBACK_PROJECT")
        token = os.environ.get("GITLAB_FEEDBACK_TOKEN")
        if not project or not token:
            return

        print(f"Posting feedback for {self.node_id} by {self.student_id} to GitLab", flush=True)
        if self.node_id.endswith("-exam") or self.node_id.endswith("-project"):
            confidential = 'true'
        else:
            confidential = 'false'
        url = f'{base_url}/api/v4/projects/{urllib.parse.quote_plus(project)}/issues'
        result = requests.post(
            url,
            headers = {
                "PRIVATE-TOKEN": token,
            },
            data = {
                "title": f"Feedback for {self.node_id}",
                "description": comments.replace('#', '\\#'),
                "labels": "feedback",
                "confidential": confidential
            }
        )

        if result.status_code not in [200,201]:
            print(f"NodeFeedback.post_to_gitlab error: {result.text} for {url}")



@sqlalchemy.event.listens_for(NodeFeedback, 'before_insert')
def post_to_gitlab_on_insert(mapper, connection, target):
    target.post_to_gitlab()


@sqlalchemy.event.listens_for(NodeFeedback, 'before_update')
def post_to_gitlab_on_change(mapper, connection, target):
    # Only post to GitLab if the comments field is actually changing.
    # (And WTH sqlalchemy!?)
    comments_attr = [attr for attr in db.inspect(target).attrs if attr.key=='comments'][0]
    if comments_attr.load_history().has_changes():
        post_to_gitlab_on_insert(mapper, connection, target)
