from ..app import app, db, retry_commit
from .. import working_days
from flask_login import current_user
import atexit
import datetime
import os
import secrets
import base64
import sqlalchemy as sa
from pathlib import Path
from backports.cached_property import cached_property
from apscheduler.schedulers.background import BackgroundScheduler


AVATARS_DIR = os.path.join(app.config['DATA_DIR'], "avatars")
os.makedirs(AVATARS_DIR, exist_ok=True)



class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False, unique=True)
    level = db.Column(db.SmallInteger, nullable=False, default=10)
    # 10 student
    # 30 inspector
    # 50 teacher
    # 80 admin (allows impersonation)
    # 90 owner 

    short_name = db.Column(db.String, nullable=False, unique=True)
    @sa.orm.validates('short_name')
    def convert_lower(self, key, value):
        return value.lower()

    counselor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    counselees = db.relationship("User", foreign_keys=[counselor_id], backref="counselor", remote_side=[id])

    buddy_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=True)

    attempts = db.relationship("Attempt", lazy='dynamic', foreign_keys="Attempt.student_id", back_populates="student")

    current_attempt_id = db.Column(db.Integer, db.ForeignKey('attempt.id', use_alter=True))
    current_attempt = db.relationship("Attempt", foreign_keys="User.current_attempt_id")

    default_schedule = db.Column(db.ARRAY(db.String), nullable=False, default=['present','present','present','present','present'])

    avatar_name = db.Column(db.String)

    class_name = db.Column(db.String, nullable=False, default='')

    start_date = db.Column(db.String, nullable=False, default=datetime.date.today)

    is_hidden = db.Column(db.Boolean, nullable=False, default=False)

    discord_id = db.Column(db.String, nullable=True)

    # The following properties and method are required by Flask Login:
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    is_anonymous = False

    @property
    def leniency_targets(self):
        lt = LeniencyTargets.query.filter_by(student_id=self.id).order_by(LeniencyTargets.time.desc()).limit(1).first()
        return lt.targets if lt else ''

    @property
    def is_authenticated(self):
        return self.is_active

    def get_id(self):
        return str(self.id)

    @property
    def is_fake(self):
        return "@" not in self.email

    @property
    def is_student(self):
        return self.level == 10

    @property
    def is_inspector(self):
        return self.level >= 30

    @property
    def is_teacher(self):
        return self.level >= 50

    @property
    def is_admin(self):
        return self.level >= 80
        
    @property
    def is_owner(self):
        return self.level >= 90

    @property
    def avatar(self):
        if self.avatar_name:
            return f"/static/avatars/{self.avatar_name}"
        else:
            return "/static/placeholder.png"

    @property
    def avatar_path(self):
        return os.path.join(AVATARS_DIR,self.avatar_name)

    @avatar.setter
    def avatar(self, data_uri):
        # TODO: this is not transaction safe.
        if self.avatar_name:
            try:
                self.avatar_name = None
                os.unlink(self.avatar_path)
            except:
                pass

        if not data_uri:
            return

        comma = data_uri.find(';base64,')
        if comma:
            header = data_uri[0:comma]
            image = base64.b64decode(data_uri[comma+8:])
            ext = ".jpg" if header.find("/jpeg") else (".png" if header.find("/png") else None)
            if ext:
                self.avatar_name = secrets.token_urlsafe(8) + ext
                with open(self.avatar_path, "wb") as file:
                    file.write(image)
                return
        raise Exception(f"Couldn't parse avatar data url starting with {data_uri[0:40]}")


    @property
    def description(self):
        if self.is_teacher:
            return "Teacher"
        if self.is_inspector:
            return "Inspector"
        if self.current_attempt_id:
            node = curriculum.get('nodes_by_id').get(self.current_attempt.node_id)
            if not node:
                return self.current_attempt.node_id
            topic = curriculum.get('modules_by_id')[node["module_id"]]
            return ((topic.get('short') or topic.get('name')) + " ➤ " if topic.get('name') else '') + node['name']
        return 'Idle student'

    @property
    def full_name(self):
        return (self.first_name + " " + self.last_name).strip()

    @property
    def url_query(self):
        if current_user.id != self.id:
            return f"?student={self.id}"
        else:
            return ''

    @property
    def absent_days(self):
        """Returns a list with weekday numbers (0..5) when person is structural absent as key 
        and the reason as value"""
        absent_days = []
        for num,day in enumerate(self.default_schedule):
            if day in ['home_structural', 'absent_structural']:
                absent_days.append(num)
        return absent_days

    @property
    def is_present(self):
        """True when the student has logged in from within a Saxion building today."""
        time_log = TimeLog.query \
            .filter_by(date = datetime.date.today()) \
            .filter_by(user_id = self.id) \
            .first()
        return time_log != None

    def get_presence_class(self):
        time_log = TimeLog.query \
            .filter_by(date = datetime.date.today()) \
            .filter_by(user_id = self.id) \
            .first()
        if time_log == None:
            return "away"
        now = datetime.datetime.now() - datetime.timedelta(minutes=10)
        return "online" if time_log.end_time > now.time() else "today"

    @cached_property
    def performance(self):
        return Performance(self)

    @property
    @retry_commit
    def buddy(self):
        if self.buddy_id:
            return User.query.get(self.buddy_id)

        if datetime.datetime.now().time() < datetime.time(9,0):
            return

        if not self.is_active or self.is_hidden or not self.is_student or not self.is_present:
            return

        if self.current_attempt and self.current_attempt.credits > 0:
            return
            
        QUERY = sa.text("""
        select u.id
        from "user" u
        join time_log tl on tl.user_id=u.id and tl.date = :today
        left join attempt ca on u.current_attempt_id = ca.id
        where class_name = :class_name
            and is_active = true
            and is_hidden = false
            and level = 10
            and buddy_id is null
            and user_id != :user_id
            and (ca.id is null or ca.credits = 0)
        order by coalesce(
            (
                select extract(epoch from (now() - a1.start_time)) as t
                from attempt a1
                where a1.student_id = u.id
                and a1.node_id = (
                    select node_id
                    from attempt sna
                    where sna.student_id = :user_id
                    order by start_time
                    limit 1
                )
                order by t
                limit 1
            ),
            (
                select extract(epoch from (now() - a2.start_time)) as t
                from attempt a2
                where a2.student_id = :user_id
                and a2.node_id = (
                    select node_id
                    from attempt una
                    where una.student_id = u.id
                    order by start_time
                    limit 1
                )
                order by t
                limit 1
            ),
                3600*24*365
        )
        limit 1
        """)


        with db.engine.connect() as dbc:
            buddy_id = dbc.execute(QUERY,
                class_name = self.class_name,
                user_id = self.id,
                today = datetime.datetime.now().date(),
            ).scalar_one_or_none()

        print(f"{datetime.datetime.now().time()}: buddy for {self.id} will be {buddy_id or 'nobody yet'}", flush=True)

        if not buddy_id:
            return
            
        buddy = User.query.get(buddy_id)
        buddy.buddy_id = self.id
        self.buddy_id = buddy.id
        return buddy

    def query_hours(self, start_date = "1970-01-01", end_date = datetime.datetime.today(), period = 'YYYY-MM'):
        sql = sa.text("""
        select
            to_char(date, :period) as period,
            extract(epoch from sum(
                LEAST(end_time, CAST('18:00:00' AS TIME WITHOUT TIME ZONE))
                -
                GREATEST(LEAST(start_time, CAST('19:00:00' AS TIME WITHOUT TIME ZONE)), CAST('08:00:00' AS TIME WITHOUT TIME ZONE))
            ))+30*count(*) seconds
        from time_log
        where user_id = :user_id and date >= :start_date and date < :end_date
        group by period
        """)

        with db.engine.connect() as dbc:
            return dbc.execute(sql, user_id=self.id, start_date=start_date, end_date=end_date, period=period).fetchall()

    def get_attempts(self):
        if self.is_teacher: # user is teacher, so return attempt reviews
            return Attempt.query.join(Grading).filter_by(grader_id=self.id).order_by(Grading.time.desc()).limit(10).all()
        else: # else user is student, so just get the attempts
            return Attempt.query.filter_by(student_id=self.id).order_by(Attempt.start_time.desc()).limit(10).all()
    
    # Just to make REPL work more pleasant:
    def __repr__(self):
        return '<User {}: {}>'.format(self.id, self.short_name)

    def report_absent(self, reason, date = None):
        if date == None:
            date = datetime.date.today()
        if reason:
            query = sa.text("""
insert into absent_day(user_id, date, reason)
values(:user_id, :date, :reason)
on conflict (user_id, date)
do update set reason = :reason
""")
        else:
            query = sa.text("""delete from absent_day where user_id = :user_id and date = :date""")

        with db.engine.connect() as dbc:
            dbc.execute(query, user_id=self.id, date=date, reason=reason)


class LoginLink(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    secret = db.Column(db.String, nullable=False)
    
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User')

    redirect_url = db.Column(db.String)



class LeniencyTargets(db.Model):
    student_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, primary_key=True)
    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False, primary_key=True)

    teacher_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    targets = db.Column(db.String, nullable=False)



BONUS_TIME = datetime.time(9, 3)
SOME_DATE = datetime.date(2020,1,1)
MIN_START_TIME = datetime.time(8, 0)
MAX_END_TIME = datetime.time(19, 0)

class TimeLog(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = db.relationship('User')

    date = db.Column(db.Date, nullable=False, primary_key=True, index=True)

    start_time = db.Column(db.Time, nullable=False)
    end_time = db.Column(db.Time, nullable=False)
    suspicious = db.Column(db.Boolean, nullable=False, default=False)

    @property
    def hours(self):
        start = datetime.datetime.combine(SOME_DATE, min(max(self.start_time,MIN_START_TIME),MAX_END_TIME))
        end = datetime.datetime.combine(SOME_DATE, min(max(self.end_time,MIN_START_TIME),MAX_END_TIME))
        result = (end - start).total_seconds() / 3600
        if self.start_time < BONUS_TIME:
            result += 0.5
        if self.suspicious:
            result /= 2
        return result

    @staticmethod
    def get_online_user_ids():
        online_user_ids = {}
        now = datetime.datetime.now() - datetime.timedelta(minutes=10)
        time_logs = TimeLog.query.filter_by(date = now.date()).filter()
        for time_log in time_logs:
            online_user_ids[time_log.user_id] = 'online' if time_log.end_time > now.time() else 'today'
        return online_user_ids



class AbsentDay(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = db.relationship('User')
    date = db.Column(db.Date, nullable=False, primary_key=True, index=True)
    reason = db.Column(db.String, nullable=False)

    @property
    def tag_name(self):
        tag_names = {
            "sick": "Sick",
            "home_incidental": "Home",
            "absent_incidental": "Absent",
            "home_structural": "Home",
            "absent_structural": "Absent"}
        return tag_names[self.reason]

from ..models import curriculum
from .attempt import Attempt

@app.before_first_request
def save_structural_days_as_absent_day():
    """Saves an absent_day in the database when the user has an absent day in his default schedule 
    and today is a working day."""
    today = datetime.date.today()
    day_number = today.weekday()
    if not working_days.is_working_day(today):
        # If today is not a working day, then there is no need to save an absent day.
        return
    if AbsentDay.query.filter_by(date=today).first():
        # If there is already at least one absent day for today, then do not save absent days again. 
        # (mostly used to prevent double entries when restarting server during development)
        return
    for user in User.query.all():
        if user.default_schedule[day_number] != 'present':        
            absent_day = AbsentDay(
                user_id = user.id,
                date = today,
                reason = user.default_schedule[day_number])
            db.session.add(absent_day)
    db.session.commit()

def clean_current_buddy_match():
    """Clean the current buddy matches of the day"""
    print("Cleaning buddy matches", flush=True)
    query = sa.text("""update public.user 
    SET buddy_id = null 
    WHERE level = :level""")

    with db.engine.connect() as dbc:
            dbc.execute(query, level=10)

# Every weekday (mon-fri) in the middle of the night the non-present days that are in the default schedule of the user, 
# are stored as absent_day. (From: https://stackoverflow.com/a/38501429)
scheduler = BackgroundScheduler()
scheduler.add_job(func=save_structural_days_as_absent_day, trigger="cron", day_of_week='mon-fri', hour=2, minute=15, second=0)
scheduler.start()

# Every weekday (mon-fri) in the middle of the night remove all buddy's linked to a user
buddy_cleaner = BackgroundScheduler()
buddy_cleaner.add_job(func=clean_current_buddy_match, trigger="cron", day_of_week="mon-fri", hour=2, minute=0, second=0)
buddy_cleaner.start()

from .performance import Performance
from .grading import Grading
