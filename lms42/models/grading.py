from ..app import db
import datetime


class Grading(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    attempt_id = db.Column(db.Integer, db.ForeignKey('attempt.id'), nullable=False, index=True)
    attempt = db.relationship("Attempt", backref=db.backref("gradings", lazy="dynamic"))

    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)

    grader_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, index=True)
    grader = db.relationship("User", foreign_keys="Grading.grader_id")

    grade = db.Column(db.Integer, nullable=False)
    grade_motivation = db.Column(db.String)

    passed = db.Column(db.Boolean, nullable=False)

    objective_scores = db.Column(db.ARRAY(db.Float), nullable=False)
    objective_motivations = db.Column(db.ARRAY(db.String))

    needs_consent = db.Column(db.Boolean, nullable=False, default=False)
    consent_user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    consenter = db.relationship("User", foreign_keys="Grading.consent_user_id")
    consent_time = db.Column(db.DateTime)
