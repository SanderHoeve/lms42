import json
from flask_login import current_user, login_required
from ..app import db, app, get_base_url
from ..models.user import User
import os
import flask
import requests
from urllib.parse import urlencode


def get_redirection_url():
    return f"{get_base_url()}/discord/authorize"


def get_auth_url():
    base_url = "https://discord.com/api/oauth2/authorize?"
    parameters_dict = {
        "redirect_uri": get_redirection_url(),
        "response_type": "code",
        "scope": "identify",
        "client_id": os.environ.get("DISCORD_CLIENT_ID")
    }
    parameters = urlencode(parameters_dict)
    return base_url + parameters


@app.route('/discord/remove', methods=['POST'])
@login_required
def discord_remove():
    user = User.query.get(current_user.id)
    user.discord_id = None
    db.session.commit()

    print(f"Removed discord_id for user: {current_user.short_name}", flush=True)
    flask.flash("Discord account unlinked")
    return flask.redirect(f"/people/{current_user.short_name}")


@app.route('/discord/authorize')
@login_required
def discord_authorize():
    if "code" not in flask.request.args:
        flask.flash("Error in redirect url")
        return flask.redirect(f"/people/{current_user.short_name}")

    code = flask.request.args['code']
    token_url = "https://discord.com/api/oauth2/token"
    
    body = {
        "client_id": os.environ.get("DISCORD_CLIENT_ID"),
        "client_secret": os.environ.get("DISCORD_CLIENT_SECRET"),
        "grant_type": "authorization_code",
        "redirect_uri": get_redirection_url(),
        "code": code,
    }
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    
    bearer_response = requests.post(token_url, data=body, headers=headers)
    response = json.loads(bearer_response.text)

    if "access_token" not in response:
        flask.flash("Received error when retrieving token")
        return flask.redirect(f"/people/{current_user.short_name}")

    bearer_token = response["access_token"]

    info_url = "https://discord.com/api/oauth2/@me"
    headers = {"Authorization": f"Bearer {bearer_token}"}
    user_info = requests.get(info_url, headers=headers)
    discord_user_id = json.loads(user_info.text)["user"]["id"]
    
    user = User.query.get(current_user.id)
    user.discord_id = discord_user_id
    db.session.commit()
    print(f"Added discord_id: {discord_user_id} for user: {current_user.short_name}", flush=True)

    if not update_nickname(user):
        flask.flash("You are currently not in the SD42 discord")

    return flask.redirect(f"/people/{current_user.short_name}")


def update_nickname(user):
    server_response = bot_command("users/@me/guilds")
    discord_severs = json.loads(server_response.text)

    print(f"Discord servers: {server_response.text}")
    
    if not user.discord_id:
        return False

    success = False

    for discord_server in discord_severs:
        nickname = f'{user.first_name} {user.last_name}'
        json_data = {
            'nick': nickname
        }
    
        update_url = f'guilds/{discord_server["id"]}/members/{user.discord_id}'
        username_response = bot_command(update_url, 'PATCH', json_data)

        if username_response.status_code == 204:
            print(f"Set nickname {nickname} for user {user.discord_id}", flush=True)
            success = True
        else:
            print(f"Unable to set nickname {nickname} for user {user.discord_id}: {username_response.text}", flush=True)

    return success


def bot_command(endpoint, type="GET", jsonData=None):
    request_url = f'https://discord.com/api/{endpoint}'
    headers = {
        "Authorization": f'Bot {os.environ.get("DISCORD_BOT_TOKEN")}'
        }

    if type == "GET":
        response = requests.get(request_url, headers=headers)
    elif type == "POST":
        response = requests.post(request_url, headers=headers, json=jsonData)
    elif type == "PATCH":
        response = requests.patch(request_url, headers=headers, json=jsonData)

    return response