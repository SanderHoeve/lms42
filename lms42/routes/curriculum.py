from .. import utils, working_days
from ..app import db, app
from ..assignment import Assignment, get_all_variants_info
from ..models import curriculum
from ..models.attempt import Attempt, get_notifications, FORMATIVE_ACTIONS
from ..models.user import User
from ..models.feedback import NodeFeedback
from .. import email, working_days
from flask_login import login_required, current_user
from flask_wtf.csrf import generate_csrf
from pathlib import Path
from math import ceil
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import datetime
import flask
import json
import json
import os
import random
import re
import shutil
import urllib
import subprocess
import sqlalchemy

DEFAULT_GIT_IGNORE = """.venv
node_modules
build
target
data
"""

GITLAB_URL = "https://gitlab.com/saxion.nl/42/lms42/-/issues?sort=created_date&state=all&label_name[]=feedback"

LMS_DIR = os.getcwd()



# Work-around from: https://github.com/wtforms/wtforms/issues/477#issuecomment-716417410
# Should no longer be required after the next release of WTForms (after 2.3.3).
class FieldsRequiredForm(flask_wtf.FlaskForm):
    """Require all fields to have content. This works around the bug that WTForms radio
    fields don't honor the `DataRequired` or `InputRequired` validators.
    """

    class Meta:
        def render_field(self, field, render_kw):
            if field.type == "_Option" and field.name == "finished":
                render_kw.setdefault("required", True)
            return super().render_field(field, render_kw)



class SubmitForm(FieldsRequiredForm):
    finished = wtf.RadioField("Do you consider your submission finished?", choices = [
        ('yes', "Yes, I think it should be good enough"),
        ('deadline', "No, but I've exceeded the deadline"),
        ('forfeit', "No, but I'm giving up"),
        ('accidental', "No, I have started this assignment by accident"),
    ], validators=[wtv.InputRequired()], description="If you answered no, a teacher will discuss with you how to proceed and you should feel free to skip the questions below.")

    resource_quality = wtf.RadioField('What did you think about the provided learning resources?', choices = [
        (1,'Terrible'),
        (2,'Not so good'),
        (3,'Just okay'),
        (4,'Pretty good'),
        (5,'Excellent'),
    ])

    assignment_clarity = wtf.RadioField("Was it clear what the assignment wanted you to do?", choices = [
        (1,'Not at all'),
        (2,'No'),
        (3,'A bit vague'),
        (4,'Mostly clear'),
        (5,'Perfectly clear'),
    ])

    difficulty = wtf.RadioField("How difficult did you find the assignment?", choices = [
        (1,'Way too easy'),
        (2,'Too easy'),
        (3,'Just right'),
        (4,'Too hard'),
        (5,'Way too hard'),
    ])

    hours = wtf.IntegerField("How many (effective) hours did you spent on the assignment and learning resources?")

    fun = wtf.RadioField("Did you enjoy working on the assignment?", choices = [
        (1,'Hated every moment'),
        (2,'No'),
        (3,'It was okay'),
        (4,'For the most part'),
        (5,'It was fun!'),
    ])

    comments = wtf.TextAreaField(app.jinja_env.filters['safe'](
        f"Any hints to help us improve? Anything you type here will be posted as a public (but anonymous to anyone but the teachers) <a href='{flask.escape(GITLAB_URL)}' target='_blank'>GitLab issue</a>."
    ), render_kw={"placeholder": "Your comment will be saved here until you submit the assignment."})

    submit = wtf.SubmitField('Submit attempt')



class EmptySubmitForm(FieldsRequiredForm):
    submit = wtf.SubmitField('Submit attempt')



@app.route('/curriculum', methods=['GET'])
def curriculum_get():
    student = get_student()
    periods = curriculum.get_periods_with_status(student)

    # Find the appropriate period tab to start with
    initial_block = "1.1"
    for block_name, block in periods.items():
        for topic in block:
            if topic.get("status") == "progress":
                initial_block = re.sub('[a-z]', '', block_name)
                break

    merged_periods = {}
    for block_name, period in periods.items():
        period_name = re.sub('[a-z]', '', block_name)
        if period_name not in merged_periods:
            merged_periods[period_name] = []
        merged_periods[period_name].append(period)
    

    return flask.render_template('curriculum.html',
        student=student,
        merged_periods=merged_periods,
        nodes_by_id=curriculum.get('nodes_by_id'),
        modules_by_id=curriculum.get('modules_by_id'),
        initial_block=initial_block,
        errors=curriculum.get('errors'),
        warnings=curriculum.get('warnings'),
        stats=curriculum.get("stats") if current_user.is_authenticated and current_user.is_inspector else None,
    )



@app.route('/attempts/<attempt_id>/approve', methods=['POST'])
@utils.role_required('teacher')
def approve_attempt(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    if attempt.status == "awaiting_approval":
        attempt.status = "in_progress"
        db.session.commit()
    else:
        flask.flash("Approval was not needed.")
    return flask.redirect("/inbox")



@app.route('/attempts/<attempt_id>/consent', methods=['POST'])
@utils.role_required('teacher')
def consent_grading(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    grading = attempt.latest_grading

    if attempt.status != "needs_consent":
        flask.flash("No consent is required.")

    elif grading.grader_id == current_user.id:
        flask.flash("You cannot give consent to a grade you determined yourself.")
    
    else:
        grading.needs_consent = False
        grading.consent_user_id = current_user.id
        grading.consent_time = datetime.datetime.now(tz=datetime.timezone.utc)

        ao = Assignment.load_from_directory(attempt.directory)
        grade, passed = ao.calculate_grade(grading.objective_scores)
        attempt.status = "passed" if passed else "failed"

        db.session.commit()

        flask.flash(f"Remember to publish the grade (a {grade}) in Bison! Don't finalize it yet. Work submitted on {utils.utc_to_display(attempt.submit_time)}.")


    return flask.redirect("/inbox")



@app.route('/curriculum/<node_id>', methods=['POST'])
@login_required
def node_action(node_id):
    if "start" in flask.request.form:
        return start_node(node_id)    
    elif "submit" in flask.request.form:
        return submit_node(node_id)
    else:
        raise Exception(f"Invalid action {flask.request.form}")



def start_node(node_id):
    node = curriculum.get_node_with_status(node_id, current_user)
    errors, _ = check_node_startable(node)
    if current_user.current_attempt:
        errors.append("You are already working on an assignment.")
    if errors:
        flask.flash(" ".join(errors))
        return flask.redirect(flask.request.url)
    
    last_number = db.session.query(Attempt.number) \
        .filter_by(student_id=current_user.id, node_id=node["id"]) \
        .order_by(Attempt.number.desc()).first()
    if last_number: # For some reason, a tuple is returned.
        last_number = last_number[0]
    else:
        last_number = 0

    deadline = None
    if not node.get('allow_longer'):
        deadline = datetime.datetime.now()
        if 'ects' in node:
            # Some time on the day of the deadline, in local time:
            deadline += datetime.timedelta(days=(node['days']-1))
            # 17:30 on the day of the deadline, in local time:
            deadline = deadline.replace(**working_days.DEADLINE_TIME)
        else:
            half_days = node['days'] * 3 # set deadline to 3 half-days for each recommended day of work
            half_days -= 0 if deadline.hour >= 14 else 1 if deadline.hour >= 10 else 2 # subtract today's half days
            delta_days = ceil(half_days / 2)

            if delta_days:
                day = working_days.offset(deadline.date(), delta_days)
                deadline = datetime.datetime(day.year, day.month, day.day)

            if half_days % 2 == 1:
                deadline = deadline.replace(**working_days.LUNCH_TIME)
            else:
                deadline = deadline.replace(**working_days.DEADLINE_TIME)

        # Convert to UTC
        deadline = utils.local_to_utc(deadline)

    # Select a semi-random variant giving preference to variants that the student has
    # attempted the least often.
    variant_occurrences = {}
    variant_id = 1
    while node.get(f'assignment{variant_id}'):
        # By default, take the first assignment first, unless it's an exam, than take a random assignment
        variant_occurrences[variant_id] = random.uniform(0, 1) if "ects" in node else variant_id/10
        variant_id += 1

    if not variant_occurrences:
        flask.flash("Sorry, there is no assignment available. Talk to a teacher!")
        return flask.redirect(flask.request.url)

    last_attempt = None
    attempts = Attempt.query.filter_by(student_id=current_user.id, node_id=node["id"]).order_by(Attempt.number)
    for attempt in attempts:
        last_attempt = attempt
        if attempt.status != "repair" and  attempt.variant_id in variant_occurrences:
            variant_occurrences[attempt.variant_id] += 1

    variant_id = sorted(variant_occurrences.items(), key=lambda x: x[1])[0][0]
    assignment = node.get(f'assignment{variant_id}')

    alternatives = Assignment(assignment, node).get_alternatives()
    last_alternatives = last_attempt and last_attempt.status == "repair" and last_attempt.alternatives
    for name, options in alternatives.items():
        if last_alternatives and name in last_alternatives and last_alternatives[name] in options:
            # Try to preserve alternatives from the previous attempt to be repaired
            alternatives[name] = last_alternatives[name]
        else:
            # Select a random alternative
            alternatives[name] = random.choice(options)


    attempt = Attempt(
        student_id = current_user.id,
        student = current_user,
        number = last_number + 1,
        node_id = node["id"],
        variant_id = variant_id,
        deadline_time = deadline,
        status = "awaiting_approval" if "ects" in node else "in_progress",
        credits = node["ects"] if "ects" in node else 0,
        avg_days = node["avg_attempts"] * node["days"],
        alternatives = alternatives,
    )

    for _ in range(5):
        try:
            os.makedirs(attempt.directory)
            break
        except FileExistsError:
            attempt.number += 1
            pass
    else:
        print(f"Failed to create {attempt.directory}")
        flask.flash(f"Error! Could not create attempt directory: {attempt.directory}")
        return flask.redirect(flask.request.url)

    db.session.add(attempt)
    db.session.commit() # causes attempt.id to be available
    current_user.current_attempt_id = attempt.id
    db.session.commit()

    # Write the attempt as json
    attempt.write_json()

    # Write the assignment as json
    with open(attempt.directory+"/assignment.json", "w") as file:
        file.write(json.dumps(assignment, indent=4))

    # Strip out useles data from the node, and write it to json
    node_copy = node.copy()
    variant_id2 = 1
    while node_copy.get(f'assignment{variant_id2}'):
        node_copy.pop(f'assignment{variant_id2}')
        variant_id2 += 1
    for name in ['directory', 'attempts', 'status']:
        node_copy.pop(name)

    with open(attempt.directory+"/node.json", "w") as file:
        file.write(json.dumps(node_copy, indent=4))

    # Copy the solution if it exists
    sol_dir = f"{node['directory']}/solution{variant_id}"
    if Path(sol_dir).is_dir():
        shutil.copytree(sol_dir, attempt.directory+"/solution")

    # Copy the template if it exists
    tmpl_src = f"{node['directory']}/template{variant_id}"
    tmpl_dst = attempt.directory+"/template"
    if Path(tmpl_src).is_dir():
        shutil.copytree(tmpl_src, tmpl_dst)
    else:
        os.mkdir(tmpl_dst)

    # Create a default .gitignore file if it doesn't exist
    gitignore = f"{tmpl_dst}/.gitignore"
    if not Path(gitignore).is_file():
        with open(gitignore, "w") as file:
            file.write(DEFAULT_GIT_IGNORE)

    # Do a git init in the template directory
    subprocess.run(["git", "init", "-q"], cwd=tmpl_dst)
    subprocess.run(["git", "add", "."], cwd=tmpl_dst)
    subprocess.run(["git", "commit", "-q", "--author", "sd42 <info@sd42.nl>", "-m", "Teacher-provided template"],
        cwd=tmpl_dst,
        env={'GIT_COMMITTER_NAME': 'sd42', 'GIT_COMMITTER_EMAIL': 'info@sd42.nl'}
    )

    return flask.redirect('#new')



def submit_node(node_id):
    attempt = Attempt.query.filter_by(student_id=current_user.id, node_id=node_id, status="in_progress").first()
    if attempt:
        submit_form = SubmitForm()

        # Update the attempt state
        attempt.status = "needs_grading"
        attempt.finished = submit_form['finished'].data
        attempt.submit_time = datetime.datetime.utcnow()
        attempt.total_hours = attempt.hours

        if current_user.current_attempt_id == attempt.id:
            current_user.current_attempt_id = None

        # Store feedback
        feedback = NodeFeedback()
        submit_form.populate_obj(feedback)
        feedback.node_id = node_id
        feedback.student_id = current_user.id
        db.session.merge(feedback)

        # Flush
        db.session.commit()
        attempt.write_json()

        flask.flash("Assignment submitted!")
    else:
        flask.flash("Assignment already submitted?")
    return flask.redirect('/curriculum')



@app.route('/curriculum/<node_id>/<int:variant_id>/template.zip', methods=['GET'])
def node_get_template(node_id, variant_id):
    node = curriculum.get('nodes_by_id')[node_id]
    if node.get('public',False)==True or (current_user.is_authenticated and (node.get('public',False) or current_user.is_inspector or not node.get('ects'))):
        zip = subprocess.Popen(['zip', '-r', '-', f'template{variant_id}'], stdout=subprocess.PIPE, cwd=node["directory"])
        return flask.Response(zip.stdout, content_type='application/zip')
    else:
        return 'Lesson is not public.', 403



@app.route('/curriculum/<node_id>', methods=['GET'])
def node_get(node_id):
    student = get_student()
    status = None
    node = curriculum.get_node_with_status(node_id, student)
    if not node:
        return flask.render_template('404.html', message = 'No such lesson'), 404

    pair_options = []
    assignments = {}
    if student:
        # Show all attempts for a specific student in tabs
        passed = False
        for attempt in reversed(node["attempts"]):
            regrade = int(flask.request.args.get("regrade","-1")) == attempt.id
            assignments[f"Attempt {attempt.number}"] = get_attempt_info(node, attempt, student, regrade)
            if attempt.status == 'passed':
                passed = True

        if (not assignments and node.get("public", False)=="users") or (passed and not node.get('ects')):
            variants = get_all_variants_info(node, current_user.is_authenticated and current_user.is_inspector)
            if len(variants)==1 and assignments:
                assignments["Latest"] = list(variants.values())[0]
            else:
                assignments.update(variants)

        if node["status"] != "in_progress":
            errors, warnings = check_node_startable(node)
            status = {'action': 'start', 'errors': errors, 'warnings': warnings}

        if node.get('pair') and not passed:
            # Look for students who have not finished this node, but *have* finished the node before (the node before).
            prev_node = curriculum.get_previous_node(node)
            prev_node = curriculum.get_previous_node(prev_node) or prev_node
            if prev_node:
                AttemptA = sqlalchemy.orm.aliased(Attempt)
                AttemptB = sqlalchemy.orm.aliased(Attempt)
                pair_options = User.query.filter_by(level=10, is_active=True, is_hidden=False) \
                    .filter(User.id != student.id) \
                    .join(AttemptA, sqlalchemy.and_(AttemptA.student_id==User.id, AttemptA.status=='passed', AttemptA.node_id==prev_node['id'])) \
                    .outerjoin(AttemptB, sqlalchemy.and_(AttemptB.student_id==User.id, AttemptB.status!='failed', AttemptB.node_id==node['id'])) \
                    .filter(AttemptB.student_id==None)


    elif not student and ((current_user.is_authenticated and current_user.is_inspector) or node.get('public',False)==True):
        # View assignment, not attached to a specific student
        assignments = get_all_variants_info(node, current_user.is_authenticated and current_user.is_inspector, flask.request.args.get("template"))
    else:
        status = {'action': 'login'}


    return flask.render_template('node.html',
        topic=curriculum.get('modules_by_id').get(node.get("module_id")),
        student=student,
        node=node,
        pair_options=pair_options,
        assignments=assignments,
        status=status,
        base_url=f"/curriculum/{node['id']}/static/",
    )



def get_attempt_info(node, attempt, student, force_new_grading=False):
    """View assignment attempt for a specific student."""

    result = {
        'html': '',
        'submit_warnings': [],
    }

    if current_user != student or attempt.status != "awaiting_approval":
        needs_grading = (attempt.status == "needs_grading") or force_new_grading
        show_rubrics = (True if needs_grading else "disabled") if current_user.is_teacher else False
        
        # Load the grades, if any
        latest_grading = attempt.latest_grading
        if latest_grading==None and needs_grading and current_user.is_teacher:
            # Help the teacher by prefilling the grading from a previous attempt that needed to be repaired.
            prev_attempt = Attempt.query.filter(
                Attempt.student_id == attempt.student_id,
                Attempt.node_id == attempt.node_id,
                Attempt.variant_id == attempt.variant_id,
                Attempt.status != "needs_grading",
            ).order_by(Attempt.submit_time.desc()).first()

            if prev_attempt and prev_attempt.status == "repair":
                latest_grading = prev_attempt.latest_grading

        # Render the actual assignment to HTML
        ao = Assignment.load_from_directory(attempt.directory)
        if not current_user.is_teacher and "ects" in node and attempt.status != "in_progress":
            result['html'] = "<div>Exams can only be viewed while taking them. If you'd like to review your exam and see how it was graded, ask a teacher.</div>"
            shown_grading = latest_grading if latest_grading and not latest_grading.needs_consent else None
            result['html'] += ao.render_grading(ao.assignment, ao.goals, show_rubrics, shown_grading)
        else:
            result['html'] = ao.render(show_rubrics, latest_grading, attempt.alternatives)

        result['notifications'] = get_notifications(attempt, attempt.latest_grading)

        if current_user.is_teacher:
            if needs_grading:
                result['html'] = f'''
                    <form method="post" action="/inbox/grade/{attempt.id}" >
                        {result['html']}
                        <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                        <h1>Finalize grading</h1>
                        {"" if "ects" in node else render_formative_action(attempt.status)}
                        <textarea placeholder="Motivation..." class="textarea" name="motivation">{flask.escape(latest_grading.grade_motivation) if latest_grading and latest_grading.grade_motivation else ''}</textarea>
                        <div class="buttons is-right mt-2">
                            <script src="/static/fill-not-graded.js"></script>
                            <button type="button" class="outline" onclick="fillNotGraded(this)">Fill not graded</button>
                            {'<input class="button is-warning" type="submit" name="request_consent" value="Request consent">' if "ects" in node else ''}
                            <input class="button is-primary" type="submit" name="publish" value="Publish">
                        </div>
                    </form>
                '''

                others = User.query \
                    .filter_by(is_active=True, is_hidden=False) \
                    .join(Attempt, User.id == Attempt.student_id) \
                    .filter_by(node_id=attempt.node_id, variant_id=attempt.variant_id, status='needs_grading') \
                    .filter(User.id != attempt.student_id)

                result['others'] = [(other, f"/curriculum/{attempt.node_id}?student={other.short_name}") for other in others]
                result['others_message'] = "Other node attempts in need of grading:"
            else:
                # The form is required to keep the name spaces for radio buttons separate when we're showing multiple attempts
                result['html'] = f'<form>{result["html"]}</form>'

                if attempt.status == "awaiting_approval":
                    result['notifications'].append(f'''
                        Start of this exam needs to be approved.
                        <form method="post" action="/attempts/{attempt.id}/approve" class="buttons is-right mt-2 is-primary">
                            <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                            <input class="button" type="submit" name="approve" value="Approve">
                        </form>
                    ''')
                elif attempt.status in ["needs_consent", "passed", "repair", "failed"]:
                    if attempt.status == "needs_consent":
                        consent = '<input class="button is-primary" type="submit" name="consent" value="Consent grade">'
                    else:
                        consent = ''
                    result['html'] += f'''
                        <form method="post" action="/attempts/{attempt.id}/consent" class="buttons is-right mt-2 is-primary">
                            <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                            <a class="button" href="/curriculum/{node['id']}?student={attempt.student_id}&regrade={attempt.id}">Regrade</a>
                            {consent}
                        </form>
                    '''

        if attempt.status == "in_progress" and current_user == student:
            # It's the actual student. Show submit action in case this is the current attempt.
            current_attempt = student.current_attempt

            if current_attempt and current_attempt.node_id == attempt.node_id:
                if not current_attempt.upload_time: 
                    if node.get('upload',True):
                        result['submit_warnings'].append("You haven't uploaded any work yet.")
                elif current_attempt.upload_time + datetime.timedelta(minutes=15) < datetime.datetime.utcnow():
                    result['submit_warnings'].append("Your last upload was more than 15 minutes ago.")

                # Create the submit form
                old_values = NodeFeedback.query.filter_by(node_id=attempt.node_id, student_id=student.id).first()
                result['submit_form'] = SubmitForm(obj=old_values) if node.get('feedback',True) else EmptySubmitForm()
                if result['submit_warnings']:
                    result['submit_form'].submit.label.text += " ignoring warnings"
                    result['submit_form'].submit.render_kw = {'class_': 'button is-warning'}

                if not node.get("ects"):
                    others = User.query \
                        .filter_by(is_active=True, is_hidden=False, class_name=student.class_name) \
                        .join(Attempt, User.id == Attempt.student_id) \
                        .filter(Attempt.node_id==attempt.node_id, Attempt.status.in_(['in_progress','passed'])) \
                        .filter(User.id != attempt.student_id) \
                        .limit(5)

                    result['others'] = [(other, f"/people/{other.short_name}") for other in others]
                    result['others_message'] = "Students you may want to ask for help:"

    return result



def render_formative_action(status):
    options = [f'<option value="{k}"{" selected" if k==status else ""}>{v}</option>' for k,v in FORMATIVE_ACTIONS.items()]
    return f'''
        <div class="select mb-2 mt-2">
            <select name="formative_action" required>
                <option value="">What should happen?</option>
                {"".join(options)}
            </select>
        </div>
    '''



def check_node_startable(node):
    status = node["status"]
    warnings = []
    errors = []
    if status == "startable":
        pass
    elif status == "future":
        warnings.append("At least one previous node hasn't been completed yet.")
    elif status == "needs_grading" or status == "needs_consent":
        warnings.append("You have already submitted this assignment, but it hasn't been graded yet.")
    elif status == "passed":
        warnings.append("There's no need to start this assignment again.")
    elif status == "awaiting_approval":
        errors.append("Ask a teacher to approve starting the exam, and then reload this page.")
        warnings.append("Please review the <a href='/coc#exams' target='_blank'>exam CoC</a> before you start.")
    else:
        errors.append(f"Unknown status: {status}.")

    if current_user and current_user.current_attempt and status != "awaiting_approval":
        errors.append("You are already working on another assignment.")

    if not node.get("startable", True):
        errors.append("You don't need to 'start' this lesson.")
    elif node.get("start_when_ready"):
        warnings.append("As you'll want to interleave other modules with your work on this assignment, you should delay starting the attempt until you're ready to submit your work.")

    if node.get('wip'):
        errors.append("Sorry, this lesson is not ready yet. Please talk to your teachers!")

    return errors, warnings



@app.route('/curriculum/<node_id>/static/<name>', methods=['GET'])
def node_file(node_id, name):
    node = curriculum.get('nodes_by_id')[node_id]
    if node.get('public') != True and not current_user.is_authenticated:
        return "Permission denied", 403
    dir=f"{LMS_DIR}/{node['directory']}/static"
    return flask.send_from_directory(dir, name)



def get_student():
    if current_user:
        student_id = flask.request.args.get('student')
        if student_id and current_user.is_teacher:
            if student_id.isdigit():
                return User.query.get(student_id)
            else:
                return User.query.filter_by(short_name=student_id).first()
        if current_user.is_authenticated and not current_user.is_inspector:
            return current_user
