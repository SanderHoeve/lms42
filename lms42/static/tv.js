(function(){
    let nextE = document.createElement('h2');
    document.currentScript.after(nextE);

    let nowE = document.createElement('h1');
    document.currentScript.after(nowE);

    document.currentScript.remove();


    if (Notification.permission != 'granted'){
        Notification.requestPermission();
    }

    let timesString = `
    08:30 - 09:00: Relaxed work
    09:00 - 09:15: Kick-off
    09:15 - 10:00: Focussed work
    10:00 - 10:15: Break
    10:15 - 11:00: Focussed work
    11:00 - 11:15: Break
    11:15 - 12:00: Focussed work
    12:00 - 12:45: Lunch break
    12:45 - 13:30: Focussed work
    13:30 - 13:45: Break
    13:45 - 14:30: Focussed work
    14:30 - 14:45: Break
    14:45 - 15:30: Focussed work
    15:30 - 17:30: Relaxed work (or coding session)
    `;
    
    let timetable = [];
    for(let line of timesString.trim().split("\n")) {
        let [times, description] = line.trim().split(': ');
        times = times.split(' - ');
        timetable.push({start: times[0], end: times[1], description: description});
    }

    function calculateTimeDelta(from, to) {
        from = from.split(':').map(a => parseInt(a));
        to = to.split(':').map(a => parseInt(a));
        return (to[0] - from[0])*60 + to[1]-from[1] + " minutes";
    }

    function bell(count) {
        for(let i=0; i<count; i++) {
            setTimeout(function() {
                new Audio('/static/bell.mp3').play()
            }, i*500);
        }
    }

    function notify(now){
        new Notification(now.description, {
            body: now.description + " until " + now.end,
            icon: '/static/favicon.svg'
        });
    }

    function update() {
        var dateTime = new Date();
        time = ("0"+dateTime.getHours()).slice(-2) + ":" + ("0"+dateTime.getMinutes()).slice(-2);

        let nowIndex = 0;
        while(nowIndex+1<timetable.length && time >= timetable[nowIndex+1].start) {
            nowIndex++;
        }

        now = nowIndex < 0 ? null : timetable[nowIndex];
        next = nowIndex + 1 >= timetable.length ? null : timetable[nowIndex+1];

        nowE.innerText = now ? now.description : '';
        nextE.innerText = next ? next.description+" in "+calculateTimeDelta(time, next.start) : "Next: have a great evening!";

        if (now && time==now.start) {
            if (Notification.permission == 'granted'){
                notify(now);
            }
            bell(timetable[nowIndex].description == "Focussed work" ? 3 : 2);
        }

        setTimeout(update, (60.2 - dateTime.getSeconds())*1000);
    }

    update();

    // Reload every 30m
    setTimeout(function() {
        location.reload();
    }, 30*60*1000)

})();
