name: Alternative designs and user studies
description: The most obvious UX design is usually not the only or the best possible design. Explore alternatives, and do a small user study to choose between them.
goals:
    usertesting: 1
    ux-design: 1
days: 2
resources:
  -
    link: https://www.nngroup.com/videos/user-testing-jakob-nielsen/
    title: "User Testing: Why & How (Jakob Nielsen)"
    info: Watch the video on user testing.

assignment:
  - Introduction: |
      So far we have created a visual representation of applications we want to build. The question is whether users understand how the application works. Your wireframes secretly contain many assumptions you have on how the application works and in this assignment we will test whether your assumptions are correct by performing a user test. Use the wireframes you have created in the previous assignment for the board game extension application for you user test.

      - [Application Request: Board game extension](https://video.saxion.nl/media/Board%20game%20extension/1_wwqkcxm8)


  - Formulate tasks for the user to perform:
    - 
      text: |
          As explained in the video you cannot simple ask a user what he or she thinks of you wireframes. You will need to think of relevant tasks a user needs to perform in your application and get user feedback on these tasks. 
          
          Your tasks:
          - Think of at least 3 tasks the user needs to perform in your "application". 
          - Write these tasks down in the `user_test.md` file located in the template folder.

      0: No tasks formulated.
      2: One relevant tasks.
      4: Relevant tasks.
      map:
        usertesting: 1

  - Find "representative" users:
    -
      must: true
      text: |
        Before you can do the actual user test you need to find some users. Contrary to the first requirement in the video (use representative users) we do not have access to users in our target group. You do have many fellow students you can use for your user test. 
        
        Your tasks:
        - Find **at least two** students for which you will conduct the user test on.
        - These students should have not done this assignment before. This way they can provide more open feedback.
        - Tell your subject the task you that he or she needs to perform and ask them to talk out loud on what they think of the application. Write down the insights.
        - After the user has finished a task ask question of how well the application supports the user in achieving this task. Write down the insights.
  
  - Draw your conclusions:
    - 
      text: |
        After you have conducted the user test on your fellow students it is time to draw some conclusions. These conclusion could lead to issues of your design that need to be resolved. Propose improvements to your design based on these conclusions. 

        Your tasks:
        - Analyze the findings for each of the tasks in the user test. 
        - Write down (in the `user_test.md` file) how your wireframes can be improved to allow users to better perform the tasks.

        *Note*:
        Although your wireframes could be flawless and intuitive from the beginning it is highly unlikely that you will have no design improvement. Please be critical of your own work. 

      0: No tasks formulated.
      2: One relevant tasks.
      4: Relevant tasks.
      map:
        ux-design: 1

