name: Android Project
description: Create an awesome Android app of your own choosing!
days: 3
ects: 5
type: project
public: users
miller: sh
goals:
    java:
        apply: 1
        title: Program in Java.
    multithreading:
        apply: 1
        title: Write multithreaded programs.
    android-basics:
        apply: 1
        title: Create and control Views and work with Android APIs.
    android-layouts:
        apply: 1
        title: Use Android Studio to design ConstraintLayouts suitable for any screen size.
    android-navigation:
        apply: 1
        title: Create and spawn Android activities, carefully managing their lifecycles.
    android-lists:
        apply: 1
        title: Create lists with and without a custom view on Android.
    android-network:
        apply: 1
        title: Exchange data with an HTTP server on Android.
    android-database:
        apply: 1
        title: Use Android Room to store and retrieve local data.
assignment:
    - |
            Create an Android application based on an idea of your own! While designing your app, study the objectives below carefully, to make sure it ticks all of the boxes.

    - |
        **Note:** This is an exam *project*. As opposed to regular exams, you're allowed to:

        - Get a bit of help from the teachers.
        - Talk about the project with your class mates, and show them a demo.
        - Keep your source code, and do with it whatever you like. (Continue working on it? Open Source it? Impress your future employer? Build a billion dollar business around it?)

    -
        ^merge: feature
        title: Navigation
        map:
            android-navigation: 1
            java: 1
        text: |
            Your app should have at least three (very) different activities. Intent extra's should be used to have an activity show specific data. Back navigation should work as expected. Where applicable, activities should have working *up* navigation. Screen orientation changes should never loose state.

    -
        ^merge: feature
        title: Layout
        map:
            android-layouts: 1
        text: |
            The app should have a layout that looks good (conforming to the basic design principles) and works nicely on Android devices with all common screen sizes, in both portrait and landscape mode. (We don't require *response design*, just a sensible *fluid layout*.)

            The app should have customized theme colors and a custom app icon. It should use *styles* for applying commonly used View attributes.


    -
        ^merge: feature
        title: Lists
        map:
            android-lists: 1
            java: 1
        text: |
            Your app should display at least two (efficiently implemented) lists, and at least one custom adapter.

            At least one of the lists should live-update while its Activity is active (either based on user interaction, a timer, or network events).

    -
        ^merge: feature
        title: Networking
        map:
            android-network: 1
            multithreading: 1
            java: 1
        text: |
            Fetch data from the internet, presumable through an API (though web scraping may also be an option). *(2/4 points)*

            Transmit data from your app to somewhere on the internet as well. *(1/4 points)*

            <div class="notification">You may want to write a simple API server for this. Don't spend too much time on that part though, as it won't be graded. You may also be able to use our very own <em>RESTest</em> server as a mock backend. It is available at <code>https://restest.sd42.nl/</code>. Please refer to the <a href="https://gitlab.com/frankvanviegen/restest" target="_blank">documentation</a>.</div>

            Make sure your application does not block the user interface, handles errors (no internet connection, for example) with some grace, and (bonus!) displays a loading indicator. *(1/4 points)*.

    -
        ^merge: feature
        title: Room database
        map:
            android-database: 1
            multithreading: 2
            java: 1
        text: |
            Your app should store local data in a Room database. It should have at least two tables. The DAO should have insert, update and delete methods, and at least one select query that does a join.

            Database queries should never block the user interface thread. 

    -
        ^merge: feature
        title: Views and APIs
        map:
            android-basics: 1
            java: 1
        text: |
            Your app should use `TextView`, `ImageView`, `EditText`, `ListView` and at least three other types of `View`s (not counting *View Groups*).

            *The rest of this objective requires you to study new concepts. It's probably best to start working on these only in case you have time left.*
            
            Try to integrate one more complex `View`, such as a `Spinner`, an `AutoCompleteTextView` or a `ViewPager`.

            Your app should also integrate with with the wider Android system, for example by:
            
            - Using the GPS sensor, the tilt sensor, or some other sensor.
            - Integrating with the camera or gallery app to import photos into your app. Or integrating with another third-party app.
            - Allowing other apps to share something to your app (text or an image) by receiving an intent.
            - Do something (useful) in the background using a *background service*, and perhaps a timer and a Android notifications.

