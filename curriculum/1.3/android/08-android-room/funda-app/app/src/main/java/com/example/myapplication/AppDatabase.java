package com.example.myapplication;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.myapplication.models.Bid;
import com.example.myapplication.models.House;

@Database(entities = {House.class, Bid.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {
    public abstract HouseDao houseDao();

    static AppDatabase instance;

    static synchronized public AppDatabase getInstance(Context context) {
        if (instance==null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, "database-name")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}
