name: Multithreading
pair: true
goals:
    multithreading: 1
assignment:
    Introduction:
        -
            link: https://www.youtube.com/watch?v=0KAGazeMZ2o
            title: What is Multithreading?
    Password cracker:
        - |
            Provided in the `Cracker` class is a 'password cracker'. Let's say you forgot your password, but you still have its SHA1 hash (stored by some misguided application that probably should have been using something like `bcrypt` instead). You also remember that your password consists of just 5 lower case letters (which reflects badly on your security awareness).

            The cracker works by just trying (brute-forcing) all possible passwords, and checking to see if the resulting SHA1 hashes matches the one we're searching. The single-threaded implementation is provided and working. Your job is to speed things up, by making proper use of all of those CPU cores you bought.
        -
            link: https://www.youtube.com/watch?v=YdlnEWC-7Wo
            title: "Java Multithreading: Starting Threads (Video Tutorial Part 1)"
        -
            link: https://www.youtube.com/watch?v=_aNO6x8HXZ0
            title: "Java Multithreading: Volatile - Basic Thread Synchronization (Video Tutorial Part 2)"
        -
            ^merge: feature
            title: ThreadedCracker
            text: |
                Implement `ThreadedCracker` using the Java `Thread` class to speed up the cracker.

                To do that, you need to split up the work. The easiest way is to create 26 threads, one for each possible first letter. If one of the threads recovers the passwords, it can communicate it back to the main thread by writing it in an instance variable of the class.
        -
            link: https://www.youtube.com/watch?v=KUdro0G1BV4&list=PLBB24CFB073F1048E&index=5
            title: "Java Multithreading: Thread Pools (Video Tutorial Part 5)"
        -
            link: https://www.youtube.com/watch?v=lnbWFV4b7M4&list=PLBB24CFB073F1048E&index=13
            title: "Java Multithreading: Callable and Future (Video Tutorial Part 13)"
        -
            ^merge: feature
            title: ExecutorCracker
            text: |
                Implement `ThreadedCracker` using `Executors.newFixedThreadPool` with 8 threads. Communicate back the recovered password (or `null` if a thread did not find the password) using a `Future`.
    Busy bank:
        - |
            Imagine a bank that's in quite a bit of trouble: due to ransomware, the database containing all account balances is no longer accessible. As can be expected, the bank's clients are *not* happy.

            Fortunately, the bank still has a full log of all transactions that ever happened. By applying all of these transactions starting from a balance of 0 for each client, we should be able to calculate the current balances. 

            Of course, as the bank is currently out-of-service, we want this done quickly! Could you please help out the bank's programmers by applying multithreading?.
        -
            link: https://www.youtube.com/watch?v=lotAYC3hLVo&list=PLBB24CFB073F1048E&index=3
            title: "Java Multithreading: Synchronized (Video Tutorial Part 3)"
        -
            ^merge: feature
            title: ContendedBank
            text: |
                Implement `ContendedBank` using multiple `Thread`s.
                
                You'll notice that when you do this naively, you'll arrive at the wrong answer (as pointed out to you be the `checkBalances` method). Apply `synchronized` to fix this.

                Notice that performance will be (far) worse than the simple single-threaded implementation. Try to explain *why* in the comments.
        -
            link: https://www.youtube.com/watch?v=8sgDgXUUJ68&list=PLBB24CFB073F1048E&index=4
            title: "Java Multithreading: Lock Objects (Video Tutorial Part 4)"
        -
            link: https://www.youtube.com/watch?v=ghCUBi41bGA&list=PLBB24CFB073F1048E&index=11
            title: "Java Multithreading: Deadlock (Video Tutorial Part 11)"
        -
            ^merge: feature
            title: ConcurrentBank
            text: |
                Implement `ConcurrentBank` based on your `ContendedBank` implementation, but using a separate lock object for each account balance. So you'll need to create as many lockable objects as there are accounts.

                Please explain in your comments how this helps your code to run faster.

                *Hint*: Deadlocks can be prevented by always taking locks in a *fixed order*.
