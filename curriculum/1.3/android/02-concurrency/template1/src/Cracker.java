import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;

abstract public class Cracker {

    static final int PASSWORD_LENGTH = 5;

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException, ExecutionException, InterruptedException {
        while(true) {
            // Read a password from the user
            Scanner scanner = new Scanner(System.in);
            System.out.print("Type a password (" + PASSWORD_LENGTH + " a-z characters): ");
            String password = scanner.nextLine();

            // Generate and display the hash
            MessageDigest digest = MessageDigest.getInstance("SHA1");
            byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
            System.out.println("Password hash: " + bytesToHex(hash));

            // Select a Cracker
            Cracker cracker;
            System.out.print("\n1) sequential\n2) threaded\n3) executor\nYour choice: ");
            String method = scanner.nextLine();
            if (method.equals("1")) cracker = new SequentialCracker();
            else if (method.equals("2")) cracker = new ThreadedCracker();
            else if (method.equals("3")) cracker = new ExecutorCracker();
            else continue;

            // Brute-force the hash
            long before = System.currentTimeMillis();
            byte[] result = cracker.crack(hash);
            long after = System.currentTimeMillis();

            // Show the results
            if (result == null) {
                System.out.println("Password not recovered.");
            } else {
                System.out.println("Your password is: " + new String(result, "utf-8"));
            }
            System.out.println("Execution time: " + (after - before) + "ms");
        }
    }

    /** Convert a byte array to a hexadecimal String.
     * @param hash Input byte array.
     * @return Hexadecimal representation of the input hash.
     */
    static String bytesToHex(byte[] hash) {
        return new BigInteger(1, hash).toString(16);
    }


    /** Try all possible a-z password combinations with `PASSWORD_LENGTH` characters. In case the hash for one of these
     * turns out to be `hash`, return the original.
     * This abstract method is implemented by subclasses that do so using different threading models.
     * @param hash The hash to reverse.
     * @return The original, if the hash was matched, or null otherwise.
     */
    abstract byte[] crack(byte[] hash) throws NoSuchAlgorithmException, UnsupportedEncodingException, InterruptedException, ExecutionException;



    /** A recursive method that does most of the actual brute forcing work.
     * @param digest A MessageDigest object that is used for hashing each attempted original String.
     * @param hash The hash that we are seeking to match.
     * @param password A byte array that is modified to contain tried passwords. When the method is called, the
     *                 first `offset` bytes should be considered given - for the remaining bytes, each option should
     *                 be tried.
     * @param offset See previous param.
     * @return Returns true when a reversal was found, in which case `password` will contain that reversal. Otherwise
     *         false is returned.
     */
    boolean crackRecursive(MessageDigest digest, byte[] hash, byte[] password, int offset)  {
        if (offset == password.length) { // Base case
            byte[] hash2 = digest.digest(password);
            //System.out.println("password "+new String(password,"utf-8")+" "+bytesToHex(hash)+" "+bytesToHex(hash2));
            return Arrays.equals(hash, hash2);
        }

        // Recursive case
        for (byte ch = 'a'; ch <= 'z'; ch++) {
            password[offset] = ch;
            if (crackRecursive(digest, hash, password, offset+1)) {
                return true;
            }
        }
        return false;
    }


    /**
     * The simplest Cracker implementation, using just the main thread.
     */
    static class SequentialCracker extends Cracker {
        @Override
        byte[] crack(byte[] hash) throws NoSuchAlgorithmException {
            MessageDigest digest = MessageDigest.getInstance("SHA1");
            byte[] password = new byte[PASSWORD_LENGTH];
            return crackRecursive(digest, hash, password, 0) ? password : null;
        }
    }


    /**
     * A Cracker implementation using multiple Thread objects to split up the brute force work.
     */
    static class ThreadedCracker extends Cracker {
        // As the Runnables in a Thread don't have a way to return a value, we have the thread that finds the original
        // store the original in this instance variable.
        volatile byte[] result;

        @Override
        byte[] crack(byte[] hash) throws NoSuchAlgorithmException, InterruptedException {

            // Set the result to null. This is what will be returned if none of the Threads finds an answer.
            result = null;

            // TODO
            // Create Threads that each do a part of the work.
            // One approach is to have each thread check all combinations given a certain starting letter, creating 26 threads.

            // Don't forget to join() all threads before returning.

            return result;
        }
    }


    /**
     * A Cracker implementation using a thread pool of 8 executors, and Futures to each Callable's result back to the
     * main thread.
     */
    static class ExecutorCracker extends Cracker {
        @Override
        byte[] crack(byte[] hash) throws NoSuchAlgorithmException, ExecutionException, InterruptedException {
            // TODO
            // Create a thread pool (`Executors.newFixedThreadPool`)
            // Obtain a `Future` for the result for each starting letter.
            // Read all `Future`s to check if one of them has the original password. If so, return it.
            // Shutdown the thread pool immediately when returning (once we found the password, there is no need for
            // other threads to continue the search).

            return null;
        }
    }

}
