name: Priority queues
description: Efficiently keep data in a (large) list ordered at all times using trees or heaps.
goals:
    sorted_lists: 1

assignment1:
    Priority queues:
        - Priority queues are something that you'll often encounter in complex systems. They are like lists, but optimized for getting and removing the *smallest* item (so not the item that was added first but the *smallest* item). 

        -
            link: https://aquarchitect.github.io/swift-algorithm-club/Priority%20Queue/
            title: Priority Queue
            info: Very short explanation of what a priority queue is, and what it can be used for. You can ignore the 'How to implement a priority queue' section for now.

    Quick, think of something!:
        -
            ^merge: feature
            text: |
                Within `my_priority_queue.py` module, implement a min-priority queue that you can add integers to using the `add` method, and fetch (remove and return) the smallest integer from using the `fetch_smallest` method. Try to come up with a solution by yourself, without Googling. Just get it working, without worrying about algorithmic complexity and performance. 

                Your can execute a test for your work by running:
                ```sh
                python my_priority_queue.py
                ```
            weight: 2

    Binary search trees:
        - One way to implement a priority queue that has a reasonable (O(n log n)) algorithmic complexity is to use a binary search tree.
        -
            link: https://www.youtube.com/watch?v=mtvbVLK5xDQ
            title: Binary Search Trees (BST) Explained in Animated Demo
        -
            link: http://btv.melezinek.cz/binary-search-tree.html
            title: Binary Tree Visualiser
            info: An interactive model of a binary search tree that you can play with to experience how add, remove and find operations work.
        -
            ^merge: feature
            text: |
                Within `binary_search_tree.py` implement the `add`, `has`, `is_empty` and `to_list` methods. The time complexities for each of these function should be `O(log n)`, except for `to_list` which should be `O(n)`.

                You can run a few basic test cases using:
                ```sh
                python binary_search_tree.py add
                ```

            weight: 2
        -
            ^merge: feature
            text: |
                Within `binary_search_tree.py` implement the `remove` method. 

                You can run a few basic test cases using:
                ```sh
                python binary_search_tree.py remove
                ```

                The remove methods should work for three scenarios:
                
                - Remove a leaf node. (Easy.)
                - Remove a node with one child. (Medium.)
                - Remove a node with two children. (Hardest. Hint: you may want to use the provided `fetch_smallest` function.)

                You still get some points if you don't manage to implement all three scenarios.
            1: One scenarios properly handled.
            2: Two scenarios properly handled.
            3: All scenarios handled, but with a minor mistake.
            4: Perfect.
            weight: 2
        -
            text: |
                Run the 'unbalanced' test case:
                ```sh
                python binary_search_tree.py unbalanced
                ```

                Within `unbalanced.txt`:
                - Describe in your own words why an unbalanced tree may cause problems.
                - Show what the balanced output of the above test case would look like.
                - Use a search engine to find the name and Python implementation of an algorithm that always keeps a tree balanced. Type just the name and an URL for the code in `unbalanced.txt`. Have a quick look at the implementation to get a feel for what it involves.
            weight: 1
            2: Two questions right (with a minor issue), and question wrong.
            4: Perfect.

    Min heap:
        - |
            If all you want to do with a data structure finding and removing the smallest item (while adding new unsorted items), a *binary search tree* is not the fastest (or easiest) way to do this. A *min heap* (or *max heap*, in case we're always interested in fetching the *largest* items) is the best option in that case. Let's dive in!

            But do keep in mind that *binary search trees* can be used in a lot of situations where *min heaps* would not work. For instance when you quickly need to look up the smallest value that is larger than X, or all values between A and B, trees are a great option!
        -
            link: https://www.youtube.com/watch?v=WCm3TqScBM8
            title: Introduction to Binary Heaps (MaxHeaps)
        -
            text: |
                Given the following list: `[13, 12, 4, 23, 1, 7, 9, 32, 15]`. In the file `min-heap.txt`:
                    
                - Draw a min heap tree for this list.
                - Put the list in a min heap order.
            1: No understanding, major mistakes.
            2: A little flaw in the understanding or a larger mistake.
            3: Good understanding, but one or too minor mistakes.
            4: Flawless.
        -
            link: https://www.tutorialspoint.com/python_data_structure/python_heaps.htm
            title: Python - Heaps
            info: Python has a built-in `heapq` package that provided a *min heap*. This tutorial shows you how to use it. Take note that `heapq` uses regular lists to operate on. It only applies a special ordering to this list.
        -
            ^merge: feature
            text: Implement the class in `heapq_priority_queue` using the Python built-in `heapq` package. (And no, this is not a trick, it really should be very easy!)
