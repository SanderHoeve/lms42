name: Flask basics
description: Use Flask and Jinja2 to create a dynamic web site.
goals:
    flask: 1
resources:
    -
        link: https://www.youtube.com/watch?v=LZJNj-HHfII
        title: What is HTTP?
        info: "Before we can start building a dynamic web site, we need to know about the protocol that gets sites from the server to a client: HTTP."

    -
        link: https://cs50.harvard.edu/x/2021/weeks/9/
        title: CS50 - Flask
        info: Watch up to around 1:21:25, where the instructor starts talking about databases.

    - 
        link: https://s3.us-east-2.amazonaws.com/prettyprinted/flask_cheatsheet.pdf
        title: Pretty Printed - Flask Cheat Sheet
        info: Code snippets for the most common Flask concepts, all on one A4 page. We recommend that you keep this open while doing the assignment.

    -
        link: https://flask.palletsprojects.com/en/1.1.x/quickstart/
        title: Flask Documentation - Quickstart
        info: The quickstart guide from the official Flask documentation. This would be a good place to get more information in case some concepts in the CS50 video didn't quite make sense yet.

assignment: 
    Mock-ups: |
        <wired-card>
            <div style="display: flex">
                <h3>Lingo</h3>
                &nbsp;|&nbsp;
                <wired-link href="#">Play game</wired-link>
                &nbsp;|&nbsp;
                <wired-link href="#">View history</wired-link>
            </div>
            <wired-divider></wired-divider>
            <h3>Play game</h3>
            <style>
                .guess > .correct, .attempt-correct {
                    background-color: #009c82;
                    color: white;
                }
                .guess > .misplaced {
                    background-color: #ce6d13;
                    color: white;
                }
                .guess > .wrong, .attempt-wrong {
                    background-color: #9e0000;
                    color: white;
                }
                .guess > * {
                    padding: 0.2rem 0.4rem;
                    display: inline-block;
                    font-size: 125%;
                }
                .guess {
                    margin: 0.1rem 0;
                }
            </style>
            <ol style="padding-left: 1em;">
                <li class="guess">
                    <span class="wrong">s</span><span class="wrong">n</span><span class="misplaced">a</span><span class="wrong">i</span><span class="wrong">l</span>
                </li>
                <li class="guess">
                    <span class="misplaced">t</span><span class="wrong">r</span><span class="wrong">u</span><span class="wrong">m</span><span class="misplaced">p</span>
                </li>
                <li class="guess">
                    <span class="misplaced">t</span><span class="wrong">i</span><span class="wrong">m</span><span class="wrong">e</span><span class="wrong">s</span>
                </li>
                <li class="guess">
                    <span class="wrong">s</span><span class="misplaced">t</span><span class="wrong">r</span><span class="misplaced">a</span><span class="misplaced">p</span>
                </li>
                <li class="guess">
                    <span class="correct">p</span><span class="wrong">r</span><span class="wrong">i</span><span class="correct">c</span><span class="wrong">k</span>
                </li>
            </ol>
            <div style="color: red;">
                "ploci" is not a word...
            </div>
            <wired-input placeholder="Your 5-letter guess"></wired-input>
            <wired-input type="submit" value="Guess"></wired-input>
        </wired-card>

        <wired-card>
            <div style="display: flex">
                <h3>Lingo</h3>
                &nbsp;|&nbsp;
                <wired-link href="#">Play game</wired-link>
                &nbsp;|&nbsp;
                <wired-link href="#">View history</wired-link>
            </div>
            <wired-divider></wired-divider>
            <h3>View history</h3>
            <table>
            <tr><td>train</td><td class="attempt-wrong">failed</td></tr>
            <tr><td>snake</td><td class="attempt-correct">8 guesses!</td></tr>
            <tr><td>plane</td><td class="attempt-correct">14 guesses!</td></tr>
            </table>
        </wired-card>

        <script src="/static/wired-elements.js"></script>

    Technical requirements:
        -
            title: Tools to use
            text: |
                The functional requirements above should be implemented as a web site that...
                - Is based on Python and Flask.
                - Stores information server-side in Python (global) variables.
                - Is only meant to be used by one person at a time.
                - Uses only HTML rendered by the server. So no Javascript! (Just in case you happen to already know Javascript.)
            must: true

        - |
            <div class="notification is-important">
                Storing data in global variables is <i>not</i> something actual web apps do. It causes all data to disappear when you reload your application, and wouldn't work in case you want to scale to more than one web server process (in order to support more concurrent users). We'll be using a proper database from the next assignment onwards.
            </div>

        -
            link: https://www.learnbyexample.org/python-global-keyword/
            title: Python global Keyword
            info: Explainer on how and when to use the `global` keyword.

        -
            title: Templates
            text: Your site should use a (Jinja2) HTML template for each of the two pages. Both templates should extend a *layout* template, containing all HTML that is identical for both pages.
            ^merge: feature
            code: 0

        -
            title: CSS
            text: Create a separate `.css` file, served statically and included from your *layout* template. It should cause your site to look decent, meaning that it is at least layed out consistently and with appropriate and meaningful margins.
            0: No styling whatsoever.
            1: No external stylesheet, but some (messy) styling in the HTML.
            2: External stylesheet works, but the result looks really messy.
            3: Consistent layout with appropriate and meaningful margins.
            4: Looks great!

    Functional requirements:
        -
            title: Play game
            text: |
                Implement the *Play game* screen for *Lingo*, as shown in the mock-up above.

                - The player gets 15 guesses to guess a secret 5-letter English word, randomly chosen from a dictionary by the web server. 
                - All earlier guesses are shown in a list. For each letter, the color indicates its status: red means that this letter does not occur in the secret word, orange means that this letter occurs in the word but not at this position, green means that this letter occurs in the word at this position. (The provided `guess_to_hint` function may be of help here.)
                - There is a text input and a button that allow the user to enter a new guess.
                - When the entered word is not in the dictionary, the site should refuse to process the input, and let the user know about this.
                - When the right word is entered, the same page as before should be shown (with the latest guess being all green), but instead of the guess text input form it should show:
                    - A congratulatory message.
                    - A *New game* button, that resets the game, selecting a new secret word.
                - After 15 wrong guesses, the result should be the same as explained in the previous bullet, except that the message should be sadder, of course. 

                Clicking *View history* should navigate to the other page.

                *Hint:* Keep in mind that web programming requires a different way of thinking than the imperative Python programming we have done up until now. Instead of creating a loop in which we ask the user for input multiple times, we get incoming HTTP requests to which we *must respond immediately* and then `return` program control back to Flask. Some of the requests (the POST requests) should cause us to update the program state (for this one time stored in global variables), allowing us to keep track of things like earlier guesses.
            ^merge: feature
            weight: 3

        -
            title: View history
            text: |
                Implement the *View history* screen for *Lingo*, as shown in the mock-up above. It lists all finished games, showing the the word together with the result (number of attempts or 'failed').

                Clicking *Play game* should navigate to the other page.
            ^merge: feature
            weight: 2

    Hints: |
        - Start based on the provided *Hello world*. You should be able to run it using `poetry install && poetry run flask run --reload`.
        - When you update your `.css` file, just reloading your browser may not be enough to see the changes, as the browser may be caching the old version. Either hold shift while clicking the browser's reload button (forcing a complete cache refresh), or open the browser's development tools and tick *Disable cache* in the *Network* tab (you should keep the development tools open).
