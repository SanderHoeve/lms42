Introduction: |
     The graduation portfolio consists of eight parts, corresponding with the eight *end qualifications* of the study programme. For each of these parts, the student must receive a passing grade (5.5 or higher) in order to graduate. The grades are decided upon by the examination committee (consisting of two teachers and usually one external expert representing the software industry) based on the portfolio and how the student defends it during the graduation session.
     
     A student is expected to *grow* her/his portfolio during the entire second year. The portfolio should generally consists of professional products created as part of the internships and the free projects, but professional products created in different contexts (such as a job or a hobby project) are admissable as well, as long as your exact role in their creation can be substantiated.

Graduation supervisor: |     
     At the start of the second year, one of the teachers will be appointed graduation supervisor for a student. Starting with period 2.2, he or she will coach the student with regard to the portfolio on a biweekly basis. One important aspect of the coaching is searching for opportunities to give substance to a portfolio part based on what the student is currently doing anyway. Another aspect is to provide the student with early feedback on how he/she currently stands with regard to graduation. The graduation supervisor will *green light* planning a graduation session once he/she is confident that the outcome would be satisfactory.

     <a class="button is-primary side" href="https://repo.hboictlab.nl/template/27932629">Create project</a> 
     In order to allow to supervisor to keep track on your progress, you should maintain your work-in-progress portfolio (including your internship and free project log books) as a GitLab project. The provided template GitLab project also contains a `README.md` file, meant for keeping track of your progress. Make sure you keep this up-to-date as well!


"1 → Functional design":
     -  |
          Create a functional design (of limited complexity), based on the wishes of an (internal) customer.

          Hand in the functional design for a project of your choice. We would recommend it be a (web)app with a graphical user interface. Although the exact contents of a functional design may differ depending on the project, we would generally expect to see:
          
          - An introduction and a problem statement.
          - A domain model (in the form of an ERD or a class diagram).
          - A complete set of wireframes (including behavior descriptions) for the desired functionality.
          - A description of how you validated your designs with either the client and/or some of your intended end-users, and the results of this validation.
          - A description of any help you received.

          Alternatively, you may also propose *substantial* changes to an existing functional design or product. Make sure you execute the full design process though. Talk to your graduation supervisor in case of doubt.

     -
          type: rubric
          scale: 10
          map:
               endterm_functional_design: 1
          text: |
               Grading is based on the quality of your design, correcting for the complexity of the problem and the amount of independence with which you did the work.

               | Grade | Quality (completeness, clarity, accuracy) |
               | -- | -- |
               | 1 | There is nothing of any use. One would need to start over entirely. |
               | 5 | The FD has rather many large flaws. Even when in a rush, one would need to iterate on the FD before continuing development. |
               | 6 | The FD has some large flaws, but when in a rush it could be used as a starting point for creating a TD and an implementation. |
               | 8 | The FD has some shortcomings, but for the most parts provides enough guidance for creating a TD and an implementation. |
               | 10 | The FD is complete, clear and accurate. A TD and an implementation can be created without any further knowledge of the project. |

               | Multiplier | Complexity |
               | -- | -- |
               | 0.6 | About the complexity of a blog, including sign ups, managing posts and (unmoderated) user comments. |
               | 1.0 | About as complex as a web store, including sign ups, shopping basket, checkout, payment and product recommendations. |
               | 1.2 | Clearly more complex than the above example, or about as complex but in a problem domain that is less well understood. |

               | Multiplier | Independence |
               | -- | -- |
               | 0.5 | The student contributed only about half of the work. |
               | 0.8 | Although the student did most of the work on the FD, it's creation was tightly directed by someone else. |
               | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
               | 1.2 | The student worked very independently on topics no coworker or teacher would be able to provide assistance with. |


"2 → Technical design":
     - | 
          Create a technical design (of limited complexity), based on a (part of a) functional design.

          Hand in the technical design for a project of your choice. Although the contents of a technical design may differ depending on the project, we would generally expect to see:
          
          - An introduction.
          - A short description of the functionality of the system that needs to be build (or a reference to a relevant functional design).
          - A (high level) technical overview, for example in the form of a C4 level 2 model.
          - A description of your design choices, including motivation of the choices made and alternatives that have been considered.
          - A description of how security between parts of your system is handled.
          - A description of how and why your design choices would change if extreme scalability and reliability were important concerns. 
          - A description of any help you received.

          Alternatively, you may also propose *substantial* changes to an existing system. In that case, you should start by carefully modeling the existing system, and then execute all design steps for making your changes on top of that. Talk to your graduation supervisor in case of doubt.

     -
          type: rubric
          scale: 10
          map:
               endterm_technical_design: 1
          text: |
               Grading is based on the quality of your design, correcting for the complexity of the problem and the amount of independence with which you did the work.

               | Grade | Quality (completeness, clarity, accuracy) |
               | -- | -- |
               | 1 | There is nothing of any use. One would need to start over entirely. |
               | 5 | The TD has rather many large flaws. Even when in a rush, one would need to iterate on the TD before continuing development. |
               | 6 | The TD has some large flaws, but when in a rush it could be used as a starting point for creating an implementation. |
               | 8 | The TD has some shortcomings, but for the most parts provides enough guidance for creating an implementation. |
               | 10 | The TD is complete, clear and accurate. The TD has a clear link with the functional design and an implementation (based on the TD) can be made without any further enquiry. |

               | Multiplier | Inherent complexity |
               | -- | -- |
               | 0.7 | About the complexity of a single web-server connected to a database, with two user roles and one external service. |
               | 1.0 | About the same complexity as the Technical Design Project assignment. |
               | 1.2 | Clearly more inherent complexity than the above. |

               | Multiplier | Independence |
               | -- | -- |
               | 0.5 | The student contributed only about half of the work. |
               | 0.8 | Although the student did most of the work on the TD, it's creation was tightly directed by someone else. |
               | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
               | 1.2 | The student worked very independently on topics no coworker or teacher would be able to provide assistance with. |

"3 → Implementation":
     -  |
          Implement new software or extend existing software, based on a technical design, with regard for the quality aspects reliability, readability, adaptability, maintainability, security and performance.
     
          Hand in a document describing **two** projects you have worked on, including the source code of the two projects (including version management). These can be green field projects or extensions made to existing applications (in this case you are allowed to submit a patch set). In the document you should describe the additions made to the code base. The submitted source code should have a minimum of 500 lines of code (excluding scaffolding or generated code).
     
          For each of the two projects we would generally expect to see the following:

          - A short description of the project.
          - A description of the functionality that you have added or problems you have solved. Screenshots/recordings very welcome!
          - The part of the technical design relevant for the supplied source code, if any.
          - The source code (or patch), including any documentation, testing and a complete version history (using git or some other version management tool).
          - This first version of your source code that you created by yourself, before any reviews happened. (This can be a branch named `first-version` in your project's repository.)
          - A description of any help you received.

     -
          type: rubric
          scale: 10
          map:
               endterm_implementation: 1
          text: |
               Grading is based on the quality of your implementations, with regard for quality aspects: reliability, readability, adaptability, maintainability, security and performance.
               
               | Grade | Code quality rubrics from assignments |
               | -- | -- | 
               | 1 | The code would have to be entirely rewritten in order to be accepted in a production environment.
               | 5 | Code would require major refactoring to be accepted in a production environment.
               | 6 | Code could be accepted in a production environment with some minor revision.
               | 8 | Code can be accepted in a production environment as-is.
               | 10 | Exceptionally well-structured, elegant and/or readable code, with a lot of attention to detail.

               | Multiplier | Variety |
               | -- | -- |
               | 0.5 | The code just repeats the same trick many times. For instance: it consists of only 5 similar Flask models and forms. |
               | 1.0 | The code displays a good amount of variety. For instance: it defines a model and a form, uses an API and does something algorithmic. |


"4 → Problem solving":
     - |
          Solve problems using a solid theoretical basis.

          Hand in a document describing a (technically) challenging problem you have encountered and how you have solved it. The solution to the problem should not be obvious to an experienced developer (try a teacher!), and should involve a bit of informal research and theory.
          
          We would generally expect to see:
     
          - An introduction.
          - A problem statement.
          - A description of the process you used to analyze the problem.
          - References to resources you have consulted when solving the problem.
          - A concise description of the implemented solution and alternatives you have considered.
          - A critical (if possible numerically supported) analysis of your solution's fitness. For example in terms of time and/or space complexity, or in terms of meeting some specific goal.
          - A description of any help you received.

     -
          type: rubric
          scale: 10
          map:
               endterm_problem_solving: 1
          text: |
               Grading is based on the quality of your solution, correcting for the complexity of the problem and the amount of independence with which you did the work.

               | Grade | Quality (completeness, clarity, accuracy) |
               | -- | -- |
               | 1 | The presented solutions do not solve the presented problem at all. |
               | 5 | The presented solutions are severely flawed. Little to no alternatives have been considered or an obvious alternative has been missed. |
               | 6 | The presented solutions has some large flaws, relevant alternatives have been considered but the derivation of the solution is flawed. |
               | 8 | The presented solutions has some shortcomings, but for the most parts provides enough guidance for creating a production grade implementation. |
               | 10 | The presented solutions complete, clear and accurate. An expert presented with the problem would consider similar alternatives and come to a similar conclusion. |

               | Multiplier | Complexity |
               | -- | -- |
               | 0.6 | A problem that can be solved without much of an algorithm, but by intelligently combining standard data structures. |
               | 1.0 | About as complex as the teleporting-maze assignment. |
               | 1.2 | Clearly more complex than the above example, or about as complex but in a problem domain that is less well understood. |

               | Multiplier | Independence |
               | -- | -- |
               | 0.5 | The student contributed only about half of the work. |
               | 0.8 | Although the student did most of the work analyzing the problems, the solution was tightly directed by someone else. |
               | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
               | 1.2 | The student worked very independently on topics no coworker or teacher would be able to provide assistance with. |


"5 → Technology skills":
     - |
          Hit the ground running with various commonly used programming techniques.
          
          Create a video presentation (of no more than 10 minutes) in which proudly demonstrate and tell about all the software you have created. This may include what you did in your internships, your free projects, or in any other context (except for the other *sd42* assignments).

          In case you are not the only author of a piece of software, make sure you point out very explicitly what part you contributed. For each project you demonstrate, also mention the amount of time you invested in creating it.

          The video should mostly be proof of the fact that you are able to produce significantly sized programs in a reasonable amount of time, in multiple different technological environments. It is up to you how many different projects you want to discuss in order to show this.

          We recommend that you create a screen capture, screen shot, photos or a video of every (little) thing you create during your internships and free projects, immediately after creating it. That way, you'll only have to do some [video editing](https://software.manjaro.org/package/kdenlive) and perhaps some voice-over/textual commentary at the end of the year.

     -
          type: rubric
          scale: 10
          map:
               endterm_technology_skills: 1
          text: |
               Grading is based on the amount of time that it would take a specialized professional developer to implement all of the demonstrated software.

               | Grade | Expert weeks |
               | - | - |
               | 5 | 4 weeks |
               | 6 | 6 weeks |
               | 8 | 10 weeks |
               | 10 | 15 weeks |


"6 → Self-directed learning":
     - |
          Quickly and independently learn to use new programming techniques.
     
          For each of two new skills/techniques (like a programming language, sophisticated library, framework, complex algorithm, complex tooling, etc) that you have had to learn 'on the job' (during internships, free projects, or some extracurricular activity):

          - A description of the context. What skill were you lacking, and what did you need it for?
          - An introduction to (the basics of) the skill in your own terms, in about 400 words. You may include short code examples, if that helps. The text should be targeted towards fellow students who completed the first year, but did not study this particular skill.
          - How you approached learning the skill. Ideally you would be able to refer to specific parts of your journal here.

     -
          type: rubric
          scale: 10
          map:
               endterm_self_directed_learning: 1
          text: |
               For each of the two new skills/techniques, the following rubric can be applied to construct a grade. The final grade should be the (optionally weighted) average of the two. 

               | Grade | Complexity/extensity |
               | -- | -- |
               | 1 | Nothing new. |
               | 3 | Light use of a relatively simple new library (like chart.js). |
               | 5 | Light use of a new programming language (like C), an elaborate new framework (like Django) or a complex library (like D3.js). Or extensive use of a simpler dependency. |
               | 7 | Extensive use of a new programming language *or* an elaborate new framework *or* a complex library. |
               | 9 | Extensive use of a new programming language *and* an elaborate framework/complex library on top of that. |

               | Multiplier | Novelty |
               | -- | -- |
               | 0.6 | The new technique is really similar to something in the curriculum. It's mostly just syntax and/or names that are different. Semantics are almost identical. |
               | 1.0 | The new technique is conceptually similar to something within the curriculum, but has many many small differences in semantics. (Like going from Python to Ruby.) |
               | 1.2 | The new technique is conceptually unlike anything in the curriculum. (Like going from Python to Clojure, or from SQL+REST to NoSQL+GraphDB.) |

               | Multiplier | Independence |
               | -- | -- |
               | 0.5 | The student contributed only about half of the work. |
               | 0.8 | Although the student learned the techniques a lot of guidance was needed. |
               | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
               | 1.2 | The student worked very independently on topics without the assistance of a coworker or teacher. |


"7 → DevOps":
     - |
          Automate software testing and deployment.
          
          Hand in a document in which you describe how you have set up DevOps for a web application or a network-based API. This should include (most of) the following topics:
          
          - Automated (unit and/or end-to-end) testing.
          - Automated quality checks, such as linting and test coverage.
          - Containerization.
          - Continuous integration and/or deployment.

          For each of these topics we'd like to see:

          - A description of how you set things up.
          - Source code, screenshots, or any other supporting material.
          - Your motivation for doing this. What benefits does it have for future development?

          Alternatively, you may also describe the *substantial* improvements you made to an existing DevOps system. In that case, make sure that you include a thorough description of what was already there before you stepped in, and what the new requirements were. Talk to your graduation supervisor in case of doubt.

     -
          type: rubric
          scale: 10
          map:
               endterm_devops: 1
          text: |
               | Grade | Automated testing |
               | ----- | ---------- |
               |  1 | No useful unit- nor integration-tests. |
               |  6 | Either unit- or integration-tests covering some of the critical sections. |
               |  8 | Either unit- or integration-tests covering all of the most critical code sections. |
               | 10 | Both unit- and integration-tests covering all of the most critical code sections. |

               | Grade | Automated quality checks |
               | ----- | ---------- |
               | 1 | No useful checks. |
               | 6 | Some linting and some coverage reporting is being done, but it is not well-configured and linting errors do not block commits from being pushed to *main*. |
               | 10 | Well-configured pre-commit linting and an easy way of seeing which lines/branches are covered by tests. |

               | Grade | Containerization |
               | ----- | ---------- |
               | 1 | Containers have not been used. |
               | 4 | There is a working container, but it does not provide clear value for either testing or development. |
               | 7 | Containerized automated testing (using a container registry) **or** a documented easy to setup and use development environment. |
               | 10 | Containerized automated testing (using a container registry) and a documented easy to setup and use development environment. |

               | Grade | Continuous integration and/or deployment |
               | ----- | ---------- |
               | 1 | No CI/CD. |
               | 4 | Continuous testing on the *main* branch, but no deployment. |
               | 7 | Continuous testing and deployment (if tests pass) on the *main* branch. |
               | 10 | Automated testing of pull/merge requests, clean tests + clean linting + minimum code coverage percentage are a requirement for merging into *main*, automated deployment on merge. |

"8 → Teamwork":
     - |
          Effectively collaborate as part of a development team, using modern tools and methodologies.

          Hand in a document that reflects on how you have behaved as part of a software development team.

          The document should contain:

          - For (at least) two different companies (or independent teams with very different processes within a company):
               - For each of the roles within the company that a software developer would encounter in her/his work:
                    * Describe the role and its purpose within the company (for example developer, scrum master, tester, marketeer, supervisor, HR manager, UX designer, etc).
                    * State (an estimate of) how many employees have this role within the company.
                    * Describe whether the role is full filled by a software engineer or someone with a different background (if so give a short description of what's generally the profile for this role).
                    * Describe the purpose of your collaboration. Briefly describe how often you met and what you have done to make this collaboration as smooth as possible.
               - A description (about 800 words) of the development process, focussed on the most important things the company does to achieve a high development quality and velocity. Topics may include: all kinds of regular meetings, review procedures, collaboration tools, roles, etc. 
               - Your recommendations to the company on how the development process could perhaps be improved. Make sure you account for the inherent differences between companies.
               - For both companies/teams: A written response to your process description and recommendations from a senior developer working at the company, regarding the accuracy of your description and the value of your recommendations. You may use the *internship supervisor feedback* form filled in by your internship supervisors for this.
          - A table that clearly shows the most important differences and commonalities between the processes of the above companies.
          
          Your descriptions and recommendations can be based on your own experiences while doing an internship at (or working for) the company, or can be based on interviews with developers at the company.

     -
          type: rubric
          scale: 10
          map:
               endterm_teamwork: 1
          text: |
               | Grade | Roles & process descriptions & table quality |
               | -- | -- |
               | 4 | The descriptions and comparison fail to demonstrate an understanding of multiple essential company processes. |
               | 7 | A few roles are missing, the description for the remaining ones has some issues but captures the gist. The table identifies many important differences at a glance, but is missing a few important ones and contains some irrelevant/clumsy rows. |
               | 10 | The roles and processes of both companies are thoroughly and clearly described. Differences and commonalities are very relevant and clear. |

               | Grade | Recommendation value |
               | -- | -- |
               | 1 | There are no recommendations, or they provide no insight. |
               | 5 | Although the recommendations are aimed at actual problems the company may be experiencing, they are mostly very vague or hard to implement. |
               | 7 | Though the recommendations show some inexperience, they may service as useful input to an internal discussion on how to improve the process. |
               | 10 | Thoughtful and actionable recommendations. The company would be well advised to at least take them into serious consideration. |


Journal:
     - |
          Create a journal for at least 100 working days of doing development work. For each day, the following questions should be answered:

          1. What have you accomplished today?
          2. What are the most important things you learned today?
          3. What are the most important questions you still have at the moment and/or what will be your next steps?

          On average, your answers should combine to about 100 words per day. The `journals/` directory in the template GitLab project contains empty journal files and an example. 

     -
          must: true
          text: |
               A well-formed journal is an precondition for receiving a graduation grade.


Graduation procedure:
- |
     - Once you get the *green light* from your graduation supervisor (or you decide that you're ready against his/her advice), the both of you should set a time and location for your public defense. Generally, there should be at least about 10 days between the time of planning and the time of the defense.
     - A graduation committee will be formed, consisting for two examiners (teachers) and usually one external expert. The external expert must be a senior professional software developer, whom has no ties with the student. (A student's internship supervisors in particular should *not* be appointed external expert.) The committee will study your portfolio.
     - You must submit your portfolio in its final form at least one week (168 hours) before the start of your public defense. This is done by uploading your GitLab portfolio project (including the git history) to this LMS assignment and submitting.
     - During a public defense, you will defend your portfolio, meaning that you'll be answering questions asked by the graduation committee. This may take at most 45 minutes.
     - As this is a *public* defense, classmates, family and friends are more than welcome to attend. It is customary that you start with a short presentation of your work and your results for the benefit of those that didn't read your portfolio. (This is not part of the 45 minutes, not a requirement and doesn't count for your grade.) The committee may also allow some questions to be asked by the public.
     - After the defense, the graduation committee will retreat to grade your examination work as follows:
          - Each of the graduation committee members will fill in a [individual grading form](graduation-grading-form-individual.pdf), containing the eight partial grades and motivation for them. These forms will be filed as part of the graduation dossier, but will not be shared with the student.
          - The committee will compare the forms and discuss any differences. Based on this, the [final grading form](graduation-grading-form-final.pdf) is filled in by the examiners. The external expert is only present in an advisory capacity. The motivation for each partial grade should explain how any initial differences in opinion were resolved. It should also summarize common individual motivations. The form must be signed for agreement by both examiners. The external expert may either sign for agreement, or sign for just having been heard. In the latter case, the external expert may add her/his vision and the examiners may add their reaction. The final grading form is also part of the graduation dossier. The student also receives a copy.
          - If any of the eight partial grades is lower than 5.5, the lowest will be the overall grade. Otherwise, the average will be the overall grade. In case the journal (which is a precondition) is found to be incomplete, no grade can be assigned.
     - Having completed the final grading form, the committee will rejoin the room and share the grading results and motivations with everybody present. This would generally be followed by celebrations! It is customary to invite all attendants for a couple of drinks.

-
     must: true
     text: All end term grades are at least satisfactory (5.5 or higher).
