- |
    Today we are building our own translator. Our Babelfish translator translates simple sentences into bizarre new sentences.
-   Features:
    -   Make a menu:
        -
            text: |
                The application asks the user to input a sentence and provides options for translation.
                ```
                *** Welcome to the babelfish translator ***
                Enter the text you want to translate (type 'exit' to quit the application): I am an elite hacker

                How do you want to translate the text:
                1. Translate to leet speak
                2. Translate to half speak
                3. Translate to leet Yoda speak
                4. Translate to leet Yodadada speak
                Please enter your selection: 5
                Please select a valid option (1, 2, 3, 4): _
                ```
                - The application asks the user to input a text that needs to be translated.
                - The application checks whether a valid input has been supplied.
                - The application quits the application when the user inputs 'exit'. Note that this should be case insensitive (so 'Exit' also exits the application).
            ^merge: feature
            
    -   Leet speak:
        - 
            link: https://www.youtube.com/watch?v=Ctqi5Y4X-jA
            title: How to Use Strings in Python - Python Tutorial for Beginners
            info: How to use strings in Python. Learn Python basics with this Python tutorial for beginners.
        -
            link: https://www.w3schools.com/python/python_ref_string.asp
            title: Python String Methods
            info: A list of all the functions that are available in Python for strings. 
        -
            text: |
                Create a `translate_leet` function that takes a sentence string, and returns the same sentence but with the following replacements:
                - 'elite' becomes 'leet'
                - 'hacker' becomes 'haxor'
                - 'a', 'e', 'i' and 'o' become '4', '3', '1' and '0' respectively

                Use the function to implement the *leet speak* menu option, like this:

                ```
                *** Welcome to the babelfish translator ***
                Enter the text you want to translate (type 'exit' to quit the application): I am an elite 
                
                How do you want to translate the text:
                1. Translate to leet speak
                2. Translate to half speak
                3. Translate to leet Yoda speak
                4. Translate to leet Yodadada speak
                Please enter your selection: 1

                The text has been translated to: I 4m 4n l33t h4x0r

                Enter the text you want to translate (type 'exit' to quit the application):
                ```
            ^merge: feature

    -   Half a sentence:
        - 
            link: https://www.youtube.com/watch?v=Ctqi5Y4X-jA
            title: How to Use Strings in Python - Python Tutorial for Beginners
            info: How to use strings in Python. Learn Python basics with this Python tutorial for beginners.
        -
            text: |
                Create a `translate_half` function that takes a sentence string, and returns the first half of that sentence followed by three dots. So for a string of 24 characters, the first 12 characters are taken and '...' is added. (Hint: you need to obtain the length of the string, divide it by two, and use that number when *slicing* the string.)

                Use your function to implement the *half speak* menu option, like so:

                ```
                *** Welcome to the babelfish translator ***
                Enter the text you want to translate (type 'exit' to quit the application): I am an elite hacker

                How do you want to translate the text:
                1. Translate to leet speak
                2. Translate to half speak
                3. Translate to leet Yoda speak
                4. Translate to leet Yodadada speak
                Please enter your selection: 2

                The text has been translated to: I am an el...

                Enter the text you want to translate (type 'exit' to quit the application):
                ```
            ^merge: feature

    -   Yoda speak:
            
        -
            text: |
                Create a `translate_yoda` function that takes a sentence string as its argument, and returns the same sentence but with the last word moved to beginning of the sentence. The function should use `rfind` to find the position of the right-most space character in the string. Python *list* functions such as `split` (which we will learn about in the next lesson) may *not* be used.

                Combine the use of this function and your leet speak function to implement the *leet Yoda* menu option, like this:
                ```
                *** Welcome to the leet translator ***
                Enter the text you want to translate (type 'exit' to quit the application): I am an elite hacker

                How do you want to translate the text:
                1. Translate to leet speak
                2. Translate to half speak
                3. Translate to leet Yoda speak
                4. Translate to leet Yodadada speak
                Please enter your selection: 3

                The text has been translated to: h4x0r I 4m 4n l33t
                
                Enter the text you want to translate (type 'exit' to quit the application):
                ```
            ^merge: feature
    -   More Yoda speak:
        -   
            link: https://www.learnpython.org/en/Functions
            title: Functions
            info: Functions are a convenient way to divide your code into useful blocks, allowing us to order our code, make it more readable, reuse it and save some time. Also functions are a key way to define interfaces so programmers can share their code.    
        -   
            link: https://www.youtube.com/watch?v=NE97ylAnrz4&list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-&index=12
            title: Socratica - Python Functions
            info: Functions are essential to Python programming. In this tutorial, we teach you how to create a function, and cover the two types of arguments (required arguments and keyword arguments).
        -
            text: |
                Change your `translate_yoda` function to accept an additional *optional* argument (with a default value of 1) that specifies the number of words that should be moved from the back to the front of the sentence. It should work for any (positive integer) number. The application should display an error when an invalid number has been submitted. *Hint:* this can be accomplished by repeatedly moving a single word from the back to the front of the sentence.

                Use your modified `translate_yoda` function to implement the *leet Yodadada* menu option, that moves the first *two* words to the start of the sentence, as can be seen here:

                ```
                *** Welcome to the leet translator ***
                Enter the text you want to translate (type 'exit' to quit the application): I am an elite hacker

                How do you want to translate the text:
                1. Translate to leet speak
                2. Translate to half speak
                3. Translate to leet Yoda speak
                4. Translate to leet Yodadada speak
                Please enter your selection: 4

                Please enter how many words you want to move: 2

                The text has been translated to: 4n l33t h4x0r I 4m

                Enter the text you want to translate (type 'exit' to quit the application):
                ```

            ^merge: feature

    - Punctuality:
        -
            must: true
            text: |
                Make sure that the output of your program:

                - Closely matches the provided video and example outputs.
                - Contains no spelling errors. *Hint*: Install the *Code Spell Checker* extension (by *streetsidesoftware*) for Visual Studio Code. Just take a minute to do this. Really.
                - Uses correct and easy to understand messages.
                - Uses capital letters, white spaces and punctuation in all the right places.

                This is the last assignment for which *punctuality* is explicitly listed as an objective. Your teachers will continue to reject sloppy work though.
