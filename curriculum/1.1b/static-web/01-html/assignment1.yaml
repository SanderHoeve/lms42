- |
  Let's build a website that honors some of the programming heroes of the ages! You do the HTML, we've done the CSS styling. Of course, the HTML should be valid, semantic and consistently indented.
  
- Install the 'Live Server' extension:
    text: |
      Within Visual Studio Code, install the *Live Server* extension created by *ritwickdey*, using the *Extensions* tab on the left of the window. After installing it, switch back to the *Explorer* tab, right-click `index.html` and select `Open with Live Server`. This should open the HTML document in your browser for preview in such a way, that any changes you make and save (`ctrl-s`) in Visual Studio Code will immediately be reflected in the browser.
    must: true

- The main page:
    text: |
      You should edit the provided `index.html` such that when viewed in a browser it looks **exactly** like this:

      ![](programmers.png)

      The provided `style.css` file should take care of the styling. All you need to do is provide the right (semantic) HTML. You will probably want to peek inside the `style.css` to see what tags are being styled. We haven't studied the CSS language yet, but it should be pretty to easy to spot what HTML tag are being styled. [This page](https://www.w3schools.com/css/css_combinators.asp) explains exactly what you may want to know.
      
      And of course, you'll have to properly `<link>` the CSS file from your HTML in order to apply it.
    
    0: Nothing like the original.
    1: Multiple significant parts are missing or very much wrong.
    2: At least one significant part is missing or very much wrong.
    3: Some small differences, but nothing that really detracts from the experience.
    4: A pixel perfect match.
- The privacy statement:
    text: |
      Like above, but this time we're working on the most important page of any site: the one showing the privacy statement. Try to make `privacy.html` look exactly like this:
      
      ![](privacy.png)

      *Hint:* The `small` tag can be nested.
    0: Nothing like the original.
    1: Multiple significant parts are missing or very much wrong.
    2: At least one significant part is missing or very much wrong.
    3: Some small differences, but nothing that really detracts from the experience.
    4: A pixel perfect match.
- Links:
    text: |
      All of the blue texts in our two pages should be hyperlinks. There should be four types of links:

      - Regular, external links, in the description texts for our programming heroes. For each of these, feel free to pick any (appropriate) external URL to link to. 
      - The `Privacy statement` link in the footer of the first page should link to the second page.
      - The four `Read more on Wikipedia` links should, when clicked, open a new browser tab/window where the relevant Wikipedia page is loaded. (Hint: look into the `target` attribute of `<a>`.)
      - The table of contents links on the second page should have the browser scroll to the relevant sections in the document. (Hint: look into *URI fragments*.)
    0: None of the types of links are fully working.
    1: One of the types of links is fully working.
    2: Two of the types of links are fully working.
    3: Three of the types of links are fully working.
    4: Four of the types of links are fully working.
- Compliance with standards:
    text: |
      Both your pages should pass the W3 HTML validator without issues.
    0: Many types of errors.
    1: At most 2 different types of errors.
    2: Many types of warnings, no errors.
    3: At most 2 different types of warnings.
    4: No errors nor warnings.
- Indenting:
    text: Your HTML should be consistently indented, such that humans can easily understand the structure of your document.
    map:
      html: 1
    malus: 0.75
    0: No (or apparently random) indenting.
    1: Understandability suffers due to flawed or absent indenting.
    2: A bit sloppy here and there, but generally still quite easy to understand.
    4: Flawless. A few elemenents (such as `html` and `body`) may not have indenting (to prevent too deep indent), as long as it's done consistently.
