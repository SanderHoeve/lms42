## Introduction

### 0.1

Below is an SQL block. The queries you type within such a block (such as the example query `SELECT * FROM staff` below) can be executed by putting the text cursor inside the block and press `ctrl-shift-b`.

```sql
SELECT *
FROM staff;
```

That should open a Visual Studio Code *terminal* pane showing the query results.

### 0.2

In order to write SQL queries, you'll need to know which tables exist and what columns they have. We have the special `\d` command for that. In order to see a list of tables, run this block:

```sql
\d
```

### 0.3

To see what columns a specific table has (as well as some other information about the table), you can add the table name to the `\d` command, like this:

```sql
\d staff
```



## Field selection

### 1.1
Display all the data from the `store` table.
```sql
TODO
```

Expected column count: 7
Expected row count: 2

### 1.2

Display the title, description and release year of all movies (in the `film` table).
```sql
TODO
```

Expected column types: varchar text int
Expected row count: 1000

### 1.3

Display the payment date and amount from the payment table.
```sql
TODO
```

Expected row count: 14392
Expected column types: timestamp float

### 1.4
Display the id and name of all categories.
```sql
TODO
```

Expected row count: 16
Expected column types: int varchar


## Sort and limit:

### 2.1
Display an ordered list of customer email addresses.
```sql
TODO
```

Expected row count: 599
Expected first results:
| email |
| --- |
| aaron.selby@sakilacustomer.org |
| adam.gooch@sakilacustomer.org |
| adrian.clary@sakilacustomer.org |
| agnes.bishop@sakilacustomer.org |
| alan.kahn@sakilacustomer.org |
| albert.crouse@sakilacustomer.org |
| alberto.henning@sakilacustomer.org |
| alex.gresham@sakilacustomer.org |
| alexander.fennell@sakilacustomer.org |
| alfred.casillas@sakilacustomer.org |

### 2.2
Display the id and name of all categories sorted by name in descending order.
```sql
TODO
```

Expected results:
| id | name |
| --- | --- |
| 16 | Travel |
| 15 | Sports |
| 14 | Sci-Fi |
| 13 | New |
| 12 | Music |
| 11 | Horror |
| 10 | Games |
| 9 | Foreign |
| 8 | Family |
| 7 | Drama |
| 6 | Documentary |
| 5 | Comedy |
| 4 | Classics |
| 3 | Children |
| 2 | Animation |
| 1 | Action |

### 2.3
Display the first name, last name and country of every customer sorted by country (descending) and first name (ascending).
```sql
TODO
```

Expected row count: 599
Expected first results:
| first_name | last_name | country |
| --- | --- | --- |
| Barry | Lovelace | Zambia |
| Maria | Miller | Yugoslavia |
| Max | Pitt | Yugoslavia |
| Ella | Oliver | Yemen |
| Gina | Williamson | Yemen |
| Gordon | Allard | Yemen |
| William | Satterfield | Yemen |
| Nathan | Runyon | Virgin Islands, U.S. |
| Beth | Franklin | Vietnam |
| Greg | Robins | Vietnam |

### 2.4
Display the customer id and rental time of the 10 most recent rentals. (Yes, the data is a bit dated, but who rents videos nowadays? :-))
```sql
TODO
```

Expected results:
| customer_id | rental_time |
| --- | --- |
| 393 | 2005-08-23 22:50:12 |
| 103 | 2005-08-23 22:43:07 |
| 114 | 2005-08-23 22:42:48 |
| 74 | 2005-08-23 22:26:47 |
| 14 | 2005-08-23 22:25:26 |
| 468 | 2005-08-23 22:24:39 |
| 526 | 2005-08-23 22:21:03 |
| 131 | 2005-08-23 22:20:40 |
| 121 | 2005-08-23 22:20:26 |
| 195 | 2005-08-23 22:19:33 |


## Search through the database:
### 3.1
Display the first and last name of all actors with the first name 'Groucho'.
```sql
TODO
```

Expected row count: 3
Expected column names: first_name last_name


### 3.2
Display the titles of all films with length 55 or 66 minutes ordered by release year.
```sql
TODO
```

Expected results:
| title |
| --- |
| Wolves Desire |
| Coast Rainbow |
| Charade Duffel |
| Eve Resurrection |

### 3.3
Display the title, description for all films that have the word 'Documentary' in the description, ordered by title in descending order.
```sql
TODO
```

Expected row count: 101
Expected first results:
| title | description |
| --- | --- |
| Won Dares | A Unbelieveable Documentary of a Teacher And a Monkey who must Defeat a Explorer in A U-Boat |
| Women Dorado | A Insightful Documentary of a Waitress And a Butler who must Vanquish a Composer in Australia |
| Weekend Personal | A Fast-Paced Documentary of a Car And a Butler who must Find a Frisbee in A Jet Boat |
| Waterfront Deliverance | A Unbelieveable Documentary of a Dentist And a Technical Writer who must Build a Womanizer in Nigeria |

### 3.4
Display the customer id, inventory id and rental time of all rentals that have not yet been returned (meaning they have no return time) sorted by customer id.
```sql
TODO
```

Expected row count: 181
Expected first results:
| customer_id | inventory_id | rental_time |
| --- | --- | --- |
| 5 | 1574 | 2005-08-16 20:25:30.987771 |
| 9 | 981 | 2005-08-18 14:39:36.155266 |
| 11 | 478 | 2005-08-22 10:59:43.557827 |
| 14 | 1775 | 2005-08-17 15:03:29.879678 |
| 15 | 3715 | 2005-08-19 23:01:57.480239 |
| 15 | 526 | 2005-08-19 07:48:54.524392 |
| 21 | 1186 | 2005-08-17 18:35:07.252238 |
| 22 | 3949 | 2005-08-18 16:35:55.931992 |
| 23 | 1061 | 2005-08-18 01:23:21.666826 |
| 28 | 3987 | 2005-08-19 21:02:42.858794 |

### 3.5
Display all actors that have a first name consisting of two letters, the first one being an `E`.
```sql
TODO
```

Expected row count: 3

### 3.6
Display the title, release year, rating and rental duration of all films released after 2000 and having a rating of 'NC-17' sorted by rental duration (in descending order).
```sql
TODO
```

Expected row count: 25
Expected first results:
| title | release_year | rating | rental_duration |
| --- | --- | --- | --- |
| Christmas Moonshine | 2002 | NC-17 | 7 |
| Lies Treatment | 2004 | NC-17 | 7 |
| Coneheads Smoochy | 2003 | NC-17 | 7 |


## One of a kind
### 4.1
Display all the different (unique) amounts that have been paid, ordered by amount.
```sql
TODO
```

Expected row count: 19
Expected first results:
| amount |
| --- |
| 0.0 |
| 0.99 |
| 1.98 |
| 1.99 |
| 2.99 |
| 3.98 |
| 3.99 |


### 4.2
Display all unique countries/city combinations for all customers, sorted by country and then by city.
```sql
TODO
```

Expected row count: 597
Expected first results:
| country | city |
| --- | --- |
| Afghanistan | Kabul |
| Algeria | Batna |
| Algeria | Bchar |
| Algeria | Skikda |
| American Samoa | Tafuna |


### 4.3
Display the unique ids for customers that have paid for a movie rental which has been processed by staff member Mike Hillyer. You can lookup his staff id by hand and use it in your query. Show the customer ids in ascending order.
```sql
TODO
```

Expected column count: 1
Expected row count: 591


### 4.4
Display the last 5 days that staff member Jon Stephens has rented out a film. (Hint: you'll need to use the `DATE` function.)
```sql
TODO
```

Expected results:
| date |
| --- |
| 2005-08-23 |
| 2005-08-22 |
| 2005-08-21 |
| 2005-08-20 |
| 2005-08-19 |
