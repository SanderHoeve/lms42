Introduction:
    - |
        In this assignment you are provided with two business cases for which you will have to derive a data model in the form of an Entity Relationship Diagram (ERD). In addition you will have to derive one for a use case of your own.

Entity Relationship Diagrams:
    -
        link: https://www.visual-paradigm.com/guide/data-modeling/what-is-entity-relationship-diagram/
        title: "What is Entity Relationship Diagram (ERD)?"
        info: Please study this introduction up to the *Data model examples* section.

Creating ERDs with PlantUML and Markdown:
    - |
        We've been seeing (and editing) files with the `.md` extension in earlier assignments, without actually exploring what they *are*. It's called *Markdown*. Let's dive in, because we'll be using Markdown (and its PlantUML extension) to easily 'draw' ERDs using just text. And besides that, Markdown is used by software developers for just about everything that is text but not code.

        In a nutshell: Markdown is an easy way to structure plain text files (the type of files you'd create in *Notepad*) such that they can be automatically converted into nice looking documents.

    -
        link: https://manan-tank.medium.com/learn-markdown-in-5-minutes-7cc01405495e
        title: Learn Markdown in 5 minutes
        info: Markdown cheat sheet for people in a hurry.

    - |
        In order to (pre)view formatted Markdown output in Visual Studio Code, we'll be using the *Markdown Preview Enhanced* extension by *shd101wyy*. Please install it now and see what happens when you press `ctrl-shift-v` while having a `.md` file open. Also make sure your Manjaro has the `graphviz` and `jre-openjdk` packages installed, using *Add/Remove Software*.

    -
        link: https://youtu.be/f6m-oW0YBJ8
        title: Creating ERDs in VSCode
        info: This video shows you how to quickly 'draw' ERDs from within Visual Studio Code. As this is one of your teachers talking, you should probably watch it attentively before asking said teacher for help! :-)
    -
        link: https://plantuml.com/ie-diagram
        title: PlantUML - Entity Relationship Diagram
        info: Reference documentation for creating ERDs with PlantUML. In particularly the list of cardinality symbols is good to have near.

"Case 1: StackOverflow":
    - |
        We want to build an application that stores questions and answers much like stackoverflow.com. Users can register in the application with their name and email address. Users can ask questions. A question has a title and an optional description explaining the question. Users can also respond to a question with an answer. Aside from responding to a question a user can up vote or down vote the response. Questions can be categorized in one or more categories, each containing a label and an optional description. Every time a question is viewed by a user it will be recorded (with a timestamp) so that a list of popular (most viewed) questions can be made. It should be possible to order the responses to a question by the time they were submitted.

    - Derive the entities from the use case:
        -
            link: https://www.youtube.com/watch?v=PESLfjlWVSU
            title: Identifying Entities
            info: A short video about a simple technique to identify which tables and columns you may want to create based on a case description.
        -   text: |
                List all the entities (tables) and attributes (columns) that have been mentioned in the case description in the first section of `assignment.md`.

            0: Barely anything.
            2: Largely complete and some spurious.
            4: Complete list, no spurious entities.
            map:
                datamodeling: 1
                foreignkeys: 0


    - Design a logical data model:
        -
            text: |
                Create an **Logical** Entity Relation Diagram (ERD) for your data model using PlantUML within `assignment.md`.

                For each relationship line in your diagram, add the right cardinality symbol (or a number) on both sides.

            2: Needs more work before this can be used.
            3: Some small issues, but good enough to start creating a database.
            4: Perfect.
            map:
                datamodeling: 2
                foreignkeys: 0

    - Design a physical data model:
        -
            link: https://www.essentialsql.com/what-is-the-difference-between-a-primary-key-and-a-foreign-key/
            title: Foreign and Primary Key Differences (Visually Explained)
            info: This article will teach you the difference between a primary key and a foreign key. This article will also teach you why both of these keys are important when it comes to the maintenance of a relational database structure.

        -
            text: |
                Create a **Physical** ERD for the StackOverflow case.

            2: Needs more work before this can be used.
            3: Some small issues, but good enough to start creating a database.
            4: Perfect.
            weight: 2

"Before you continue...": |
    In the objectives below you'll be asked to design more databases. It is probably a good idea to ask a teacher or a smart-looking fellow student for some feedback on your StackOverflow design, in order not to keep making the same mistakes.

"Case 2: Twitter":
    - |
        In this use case we want to derive a physical data model for twitter. For our data model we would like keep track of users, tweets and hashtags. Our users have a handle (for example @TimothySealy), a small bio and a date that they have joined. A user can follow another user. A user can create a new tweet. An existing tweet can be retweeted or liked. The time at which the tweet was retweeted or liked is important in order to determine whether a tweet is going viral (for example many retweets and/or likes in a certain time period). A tweet can have zero or more hashtags. Hashtags only have a label.

    - Design a physical data model:
        -
            text: |
                Create a **Physical** ERD for this case.
                
            2: Needs a bit more work.
            3: One or two minor issues.
            4: Perfect.
            weight: 3

"Case 3: up to you":
    - Design a physical data model:
        -
            text: |
                Create another **Physical** ERD complete with cardinalities for a (web) application of your choice. Of course, you need to make sure your choice is not stackoverflow.com, or twitter.com, or anything too similar. ☺ The diagram should have at least six entities, twelve attributes and eight relations.
            2: Needs more work before this can be used.
            3: Some small issues, but good enough to start creating a database.
            4: Perfect.
            weight: 4
