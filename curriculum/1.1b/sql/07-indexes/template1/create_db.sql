create table person(
    id serial,
    first_name text not null,
    last_name text not null,
    gender char(1) not null,
    birth_date date,
    join_date date,
    boss_id int
);

create table department(
    id serial,
    name text not null
);

create table person_department(
    person_id integer not null,
    department_id integer not null
);
