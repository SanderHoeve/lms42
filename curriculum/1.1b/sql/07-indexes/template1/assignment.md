## Basic indexes

### 1.1

Write a query that counts the number of people with either `Lisa` or `Martin` as their first name.

```sql
TODO
```

### 1.2

Copy the above query here but modify it to use `EXPLAIN` to output a query plan.

```sql
TODO
```

Use the output of `EXPLAIN` to describe in your own words how PostgreSQL executes to above query.

TODO

How long did the query take to run?

TODO

How long would you expect the query to take if there were 10 times as many people in the database?

TODO

### 1.3

Use `CREATE INDEX` to add an index to the `people` table that speeds up your query.

```sql
TODO
```

Make sure to give your index a sensible name.

### 1.4

Copy-paste your query from 1.1 here. It should be faster now!

```sql
TODO
```

### 1.5

- Use the output of `EXPLAIN` to describe in your own words how PostgreSQL executes to query this time.
- How long did the query take to run?
- How long would you expect the query to take if there were 10 times as many people (including 10 times as many Lisas and Martins) in the database?

### 1.6

Copy-paste your query from 1.1 here again, but modify it such that it searches for people called `Truus` or `Henk`.

```sql
TODO
```

### 1.7

- How long did the query take to run?
- Why is there a difference with the time taken by 1.4?
- How long would you expect the query to take if there were 100 times as many people (assuming still no `Truus`es nor `Henk`s) in the database?



## Primary keys are indexes

### 2.1

Write a query that shows the birth date of the youngest person that is the boss of someone named 'John' (first name).

```sql
TODO
```

Expected results: 
| max |
| --- |
| 2002-12-28 |

### 2.2

The provided tables have no primary keys nor foreign keys yet. `ALTER` the tables to add them. There should be three primary keys and three foreign keys in total.

*Hint*: Junction tables generally use a *composite* primary key, consisting of both foreign ids.

```sql
TODO
```

### 2.3

Copy the query from 2.1 here:

```sql
TODO
```

### 2.4

The query should be significantly faster now. Which of the keys you added in 2.2 causes the speedup and why?



## Indexes and range queries

### 3.1

Write a query that counts how many people joined the company in June of 2010.

```sql
TODO
```

Expected results:
| count |
| --- |
| 5765 |

### 3.2

Write a query that shows the first names of the first 10 people to join the company.

```sql
TODO
```

Expected unordered results:
| first_name |
| --- |
| Enid |
| John |
| Gayle |
| Gene |
| Tracy |
| Brittany |
| Angelica |
| Everett |
| Jeffery |
| Guy |

### 3.3

Add an index that speeds up the previous two queries.

```sql
TODO
```

### 3.4

Copy the queries from 3.1 and 3.2, to show that they're faster now.

```sql
TODO
```

### 3.5

For each of the queries, explain how PostgreSQL uses your index to speed it up.

1.
2.



## Composite indexes and indexes on foreign keys

### 4.1

Write a query that displays the first name and last name of the 5 oldest persons that joined the organization in the year 1975.

*Hint:* use `EXTRACT(YEAR FROM join_date)`.

```sql
TODO
```

Expected unordered results: 
| first_name | last_name | birth_date |
| --- | --- | --- |
| Wade | Chambers | 1950-01-01 |
| Johnnie | Gamble | 1950-01-02 |
| Myra | Horn | 1950-01-03 |
| Lizzie | Saunders | 1950-01-03 |
| Helena | Spears | 1950-01-03 |

### 4.2

Add a *composite* index that speeds up the above query. Your index should help PostgreSQL with the filtering on join year (the first part of the index) *as well as* with finding the oldest 5 within that set (the second part of the index).

*Hint:* PostgreSQL allows expression (such as `EXTRACT(YEAR FROM join_date)`) to be part of an index. 

```sql
TODO
```

### 4.3

Verify that your original query now runs in just a couple of milliseconds.

```sql
TODO
```

Expected run time: 10ms
Expected unordered results: 
| first_name | last_name | birth_date |
| --- | --- | --- |
| Wade | Chambers | 1950-01-01 |
| Johnnie | Gamble | 1950-01-02 |
| Myra | Horn | 1950-01-03 |
| Lizzie | Saunders | 1950-01-03 |
| Helena | Spears | 1950-01-03 |

### 4.4

Based on query 4.1, write a query that shows the first and last names for all people whose boss is one the five oldest people that joined the organization in the year 1975.

```sql
TODO
```

Expected unordered results: 
| first_name | last_name |
| --- | --- |
| Beulah | Montoya |
| Jared | Ashley |
| Marshall | Whitaker |
| Rene | Monroe |
| Socorro | Chen |

### 4.5

Add an index that speeds up the previous query.

```sql
TODO
```

*Hint:* unlike primary keys, foreign keys are *not* automatically indexed.

### 4.6

Verify that your original query now runs in just a couple of milliseconds.

```sql
TODO
```

Expected run time: 10ms
Expected unordered results: 
| first_name | last_name |
| --- | --- |
| Beulah | Montoya |
| Jared | Ashley |
| Marshall | Whitaker |
| Rene | Monroe |
| Socorro | Chen |



## Views

### 5.1

Write a view called `chiefs` that contains the ids, first names and last names for all people that are the boss of at least one other person, and have no boss themselves.

```sql
TODO

SELECT *
FROM chiefs;
```

Expected row count: 222225
Expected column count: 3

### 5.2

Write a view called `department_chief_counts` that contains the ids, names and number of chiefs for each department, ordered by the descending number of chiefs. Use the `chiefs` `VIEW` for this.

```sql
TODO

SELECT *
FROM department_chief_counts;
```

Expected row count: 24
Expected first results:
| id | name | count |
| --- | --- | --- |
| 13 | Marketing | 5661 |
| 11 | Investor Relations | 5622 |
| 22 | Strategic Initiatives & Programs | 5617 |

### 5.3

Write a query that shows the total number of people (not only chiefs) working for the department that has the most chiefs. Use the `department_chief_counts` `VIEW` for this.

```sql
TODO
```

Expected results:
| count |
| --- |
| 62651 |

### 5.4

Just to prove to ourselves that `VIEW`s really *can* make queries easier to read (and write), please rewrite the previous query such that it no longer uses any `VIEW`s. This should be rather easy to do, based on the `VIEW` definitions you gave above and the use of subqueries.

```sql
TODO
```

### 5.5

Bonus! Use `EXPLAIN ANALYZE` on the above query, and see if you can make any sense of the query plan.

```sql
EXPLAIN ANALYZE
TODO
```

Your main conclusion from this may well be: wow, PostgreSQL is awesome! :-)

One more thing to consider: does it matter for the query plan whether you're analyzing query 5.3 or query 5.4?
