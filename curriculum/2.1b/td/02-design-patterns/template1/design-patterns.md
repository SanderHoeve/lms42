# Creational Design Patterns

## Factory Method
? *Hint*: Open Source.

## Builder
? *Hint*: Dynamic web.

## Singleton
? *Hint*: Android.


# Structural Design Patterns

## Adapter
? *Hint*: Android.

## Composite
? *Hint*: Algorithms.

## Decorator
? *Hint*: Dynamic web.

## Facade
? *Hint*: OOP.


# Behavioral Design Patterns

## Command
? *Hint*: Dynamic web.

## Iterator
? *Hint*: Python (show use of an iterator for something other than a data structure).

## Observer
? *Hint*: Android.

## State
? *Hint*: OOP.

## Strategy
? *Hint*: Algorithms.

## Template Method
? *Hint*: OOP.

## Visitor
? *Hint*: Parsers.

