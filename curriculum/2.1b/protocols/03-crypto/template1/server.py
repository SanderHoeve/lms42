import nacl
import nacl.public
import nacl.secret
import asyncio
import os
import pickle

# The port on which the server should listen
PORT = 27261

# For this one time, you're allowed to put secrets as constants in this source file.
# In actual systems, you should NOT do this, as the production secrets would end up
# on GitLab, in development environments, and who knows where else.

# The name of the file containing secrets, as a set() of strings encoded using `pickle()`.
MESSAGES_FILE = 'secrets.pickle'


# If the secrets file exists, load it. Otherwise create an empty set.
if os.path.exists(MESSAGES_FILE):
    with open(MESSAGES_FILE, "rb") as file:
        data = file.read()
        secrets = pickle.loads(data)
else:
    secrets = set()


async def handle_connection(reader, writer):
    """Handle a single client connection."""

    # Read the stream until EOF
    secret = await reader.read()

    # If the secret is not known, add it to the secrets file
    if secret in secrets:
        result = "Known"
    else:
        result = "Added"
        secrets.add(secret)
        with open(MESSAGES_FILE, "wb") as file:
            data = pickle.dumps(secrets)
            file.write(data)

    # Give the client a "Known" or "Added" answer
    writer.write(bytearray(result, 'utf-8'))
    writer.write_eof()


async def main():
    server = await asyncio.start_server(handle_connection, '0.0.0.0', PORT)
    await server.wait_closed()


asyncio.run(main())
