import re

with open('/usr/share/dict/american-english') as file:
    words = file.read().rstrip().split("\n")

print("\nPart 1")
# TODO: Look through the words, using a regular expression to match
# and print all 5 letter words starting with an 'h' and with a 'd' on
# the third position. Example: 'hedge'.

print("\nPart 2")
# TODO: Same, but now for all the words starting with a 'u', containing
# 'ea' and ending in 'ng'. Example: 'unleashing'.

print("\nPart 3")
# TODO: Same, but now for all 6 letter words that consist only of the
# letters 's' 'a' 'x' 'i' 'o' and 'n'. Example: 'onions'.

print("\nPart 4")
# TODO: Same, but now match words for any of the conditions in the above
# three parts using a *single* regular expression.
