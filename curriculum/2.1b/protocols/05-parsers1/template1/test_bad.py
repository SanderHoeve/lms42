import os
import subprocess
import sys

BAD = """
my_func(1,);
my_func(1 2);
my_func(,);
my_func(1,,2);
my_func(,1);
my_func(var a);
my_func({});
a = 3+;
a = +;
a = / 3;
a = var a;
a = 3 + + 4;
a =;
a = 3 + ();
a = (3 4)
var a =;
var a 3;
var 3;
a = 3
if () {}
if (3 {}
if 3 {}
if {}
if (1)
if (1) {}}
if (1) {} else
if (1) {} else {} else {}
if (1) lala
while () {}
while (3 {}
while 3 {}
while {}
while (1)
while (1) {}}
while (1) lala
""".strip().split("\n")

def test(script):
    with open('bad.jsss', 'w') as file:
        file.write(script+"\n")
    result = subprocess.run(['python', 'main.py', '--parse-only', 'bad.jsss'], capture_output=True)
    os.remove('bad.jsss')
    return result.returncode

if test("var a = 3;") != 0:
    print("Correct scripts (such as 'var a = 3;') should parse without errors")
    sys.exit(1)

for script in BAD:
    if test(script) == 0:
        print(f"Parser incorrectly accepted this invalid script: {script}")
