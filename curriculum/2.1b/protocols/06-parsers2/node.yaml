name: Parser generators & transpilers
goals:
    parsers: 1
    ast: 1
days: 2
assignment:
    Parser generators:
        -
            link: https://softwareengineering.stackexchange.com/questions/17824/should-i-use-a-parser-generator-or-should-i-roll-my-own-custom-lexer-and-parser
            title: Stackexchange - Should I use a parser generator or should I roll my own custom lexer and parser code?
            info: Read (part of) the accepted answer. Option 3 is the one we choose in the previous assignment. Now we're going for option 1!
        -
            link: https://www.youtube.com/watch?v=Rhqk9HYiB7Q
            title: Context-Free Grammar - Programming with Text
            info: A general introduction on grammars. You can stop watching when he starts talking about specific tools (such as Tracery) at about 11m.
    Python Lark:
        - We'll be using a Python library called Lark to do the parsing.
        - 
            link: http://blog.erezsh.com/how-to-write-a-dsl-in-python-with-lark/
            title: How to write a DSL (in Python with Lark)
        -
            link: https://lark-parser.readthedocs.io/en/latest/grammar.html
            title: Lark - Docs - Grammar Reference
        -
            link: https://lark-parser.readthedocs.io/en/latest/tree_construction.html
            title: Tree Construction Reference
        -
            link: https://lark-parser.readthedocs.io/en/latest/examples/index.html
            title: Lark - Docs - Examples for Lark
            info: Everybody loves examples!
    The Funcy Language: |
        Yours truly has invented a new programming language that will surely be taking over the world any moment now! It's called Funcy, and it resembles a [functional programming language](https://en.wikipedia.org/wiki/Functional_programming):

        - Functions are *values* that can be assigned to variables and passed as arguments to (other) functions, just like numbers and strings.
        - Everything in the language is an expression, meaning it evaluates to a value.

        As you might have expected from your teachers, there's no specification nor documentation for the language. (This might be a blessing though, because that means your implementation of the language doesn't need to conform to anything. 😉)

        The language is informally (and very loosely) specified and explained in an example piece of code, provided as `example.funcy`. Study it to see if it makes sense. If not, don't hesitate to ask your mentor or a teacher for clarification.

    Create a grammar:
        -
            ^merge: feature
            code: 0
            text: |
                Create a grammar for Funcy in the `language.lark` file. In order to apply your grammar to the example source code, use:

                ```sh
                poetry install && poetry run python transpiler.py example.funcy
                ```

                If you specified the grammar, this should output an AST (Abstract Syntax Tree) that makes sense. (After that, the program will crash because you've not done the next part of the assignment yet.) See if your AST matches the one provided in `example.output`. You may also want to study the provided `example.extended-output`, which shows the complete parse tree including all of the nodes that have just one child node. (These nodes are usually stripped from the tree by means of the `?` prefix in de Lark grammar.)

                *Hints:*

                - Functions calls in Funcy are just two or more consecutive expressions, where the value of the first expression is the function to be called, and the rest of the expressions are parameters. There can be no function calls without parameters.
                - In order to prevent ambiguous grammars (which may create undesirable parse trees), it is recommended *not* to create a single *expression* rule with many alternatives, but to instead create a chain of rules, where each rule refers only (with some exceptions) rules further down the hierarchy. Take a look at the expression grammar in [this example](https://lark-parser.readthedocs.io/en/latest/examples/calc.html#basic-calculator).

    Transpilation:
        - |
            In the last assignment, we created an *interpreter*, that executes a program by working through the AST directly.

            Interpreting a program is relatively slow though. That's one of the reasons programs are often *compiled* to machine code or to some byte code (for example in the case of Java, which compiles to Java Virtual Machine byte code).

            Another option is to compile to another programming language. We call this transpiling. A transpiler may sometimes be easier to build than an interpreter, and is also often faster to run (depending of course on the specifics of the target language and the interpreter). This is what we'll be doing in this assignment.

        -
            link: https://en.wikipedia.org/wiki/Source-to-source_compiler
            title: Source-to-source compiler
            info: Just read the introduction to get an idea of what transpilation (source-to-source compilation) is, and how it differs from 'regular' compilation.

        -
            link: https://lark-parser.readthedocs.io/en/latest/visitors.html#transformer
            title: Lark - Docs - Transformers
            info: Transformers are a handy tool Lark provides to work with the ASTs it generates. It is really well suited for building transpilers.
    Expression-only Python: |
        When converting Funcy to Python, there's a special challenge: Funcy only allows *expressions*, while Python uses *statements* for many things. The difference being that statements (such as function definition and assignment) don't result in a value.

        Fortunately, Python has alternatives that *are* expressions:

        - Instead of `a = b` we can write `a := b`, which is an [assignment expression](https://realpython.com/lessons/assignment-expressions/). It not only assigns `b` to `a`, but also evaluates to the assigned value. So you can write `print(a := b)` but not `print(a = b)`, as the `a = b` *statement* cannot be used as an *expression*.
        - Instead of the regular if/else, we can use a [conditional expression](https://stackoverflow.com/questions/394809/does-python-have-a-ternary-conditional-operator).
        - When multiple expressions need to be evaluated sequentially, we can use a *tuple* of expressions for that, and apply `[-1]` on it to use the value of the last expression as the value of the entire block of expressions.
        - Instead of defining named functions with `def` we can create anonymous (unnamed) function expressions using [lambda](https://www.w3schools.com/python/python_lambda.asp).

        As an example, let's convert a regular Python program to expression-only Python:

        ```python
        def hello():
            name = input("Name? ")
            print(f"Hello {name}!")

        hello()
        if input("Again? ") == "yes":
            hello()                    
        ```

        Expression-only Python:
        ```
        (
            hello := lambda: (
                name := input("Name? "),
                print(f"Hello {name}!"),
            )[-1],
            hello(),
            hello() if input("Again? ") == "yes" else None,
        )[-1]
        ```

        Newlines and indentation in the above program are only used for readability. This style of writing Python would allow the entire program to be on a single line though. 

    Funcy to Python:
        -
            ^merge: feature
            code: 0
            text: |
                Implement the `FuncyTransformer` class in `transpiler.py` to create to Funcy to Python transpiler.

                Your transpiler should be able to execute and run the example program as intended. Look at *Python* section in `example.output` for inspiration on how your output should look.

    Get Funcy!:
        -
            ^merge: feature
            weight: 0.25
            code: 0
            text: |
                Write a program in Funcy of your own, to test the limits of your transpiler.
