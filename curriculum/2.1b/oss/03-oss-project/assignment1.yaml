- Work on an existing Open Source project.
- |
    **Note:** This is an exam *project*. As opposed to regular exams, you're allowed to:

    - Get a bit of help from the teachers.
    - Talk about the project with your class mates, and show them a demo.
    - Keep your source code, and do with it whatever you like. (Continue working on it? Impress your future employer? Build a billion dollar business around it?)

- It is recommended *not* to work on this project in a single stretch, but to alternate with the other modules. This allows you to await response to your work/proposals from the project's community.


-   Tasks:
    -   Find an Open Source project:
            text: |
                The Open Source project should fulfill the following requirements:

                  - Contains at least 15k lines of code. [This site](https://codetabs.com/count-loc/count-loc-online.html) can help you count.
                  - Has been updated in the last two months.

            must: true

    -   Find the coding standards for the project:
        -   
            must: true
            text: Find the coding standards for the project you want to contribute to. If the project doesn't explicitly specify a standard, try to find a public standard that matches the practices used by the project.


    -   Find or create issues that you want to work on in this project:
        - |
            Find one or multiple issue in the project that you want to work on. Look for issues labeled "good first issue" or "beginner". Start with something simple.

            You can also come up with an issue of your own for the project. This can either be a bug that you want to resolve or an improvement you want to add to the project. Make sure you create a GitHub *issue* and actively ask other contributors for opinions before starting to implement your idea.

            It is recommended that you go through (at least) two distinct contribution cycles:
            
            - First for a relatively straightforward patch, possibly from a "good first issue" label. This helps you get a feel for the project and the contribution process.
            - Then a more involved patch, perhaps (but not necessarily) based on an idea of your own.

            In case of doubt if an issue is suitable: ask a teacher and/or extra nicely ask the maintainers (or 'core contributors') for advice.

    -   Implement your feature(s) and/or fixes:
            map: legacy_impl
            1: 1 out of 4
            2: 2 out of 4
            3: 3 out of 4
            4: Complex, substantial, well-executed and independent.
            text: You get points for implementing a solution (or a set of solutions) that is substantial in size and (inherent) complexity. Besides that, code quality and the level of independence calculate into this grade.

    -   Adhere to the projects coding standards and contribute upstream:
            map: legacy_contrib
            0: Coding standards and contribution guidelines were not observed.
            2: Coding standards and contribution guidelines were partly observed.
            4: Coding standards and contribution guidelines were fully observed, and the student demonstrated a collaborative attitude.
            text: |
                Note that your patch(es) don't need to be accepted (yet) in order to receive a good grade. You *should* however have taken all the appropriate steps to do your part in achieving this, demonstrating a collaborative attitude and respecting other people's time.

    -   Maintain a journal:
            text: |
                After each day that you worked on the project, write down the answers to the following three question:
                1. What have you accomplished today?
                2. What do you know now what you did not know this morning?
                3. What are the questions you (still) have at the moment?

                At the end of the project submit your journal. To get an idea of how elaborate your answers should be: aim for 100+ words per day (for the three answers combined).
            map: legacy_analyze
            0: No relevant questions have been posed during the execution of the project.
            2: Some relevant question have been posed or many question have been posed only some answered.
            4: Relevant questions have been posed and properly answered (by the student) during the execution of the project.

    -   Submitting:
            text: |
                Your LMS submission should contain:
                
                - URLs for the Pull/Merge Request(s) and (possibly) issue threads that you participated in.
                - A patch (also know as a *diff*) for each of your Pull Requests.
            must: true