name: Node.js
description: Create a web app using Node.js.
goals:
    nodejs: 1
    javascript: 1
assignment:
    Assignment:
        - |
            Let's not be shy about our goal for this assignment: You'll be creating an actual web search site (think Google!) using Node.js, NPM, Express, Handlebars, better-sqlite3 and FTS5. Yes, this is gonna be awesome! :-)
            
            But not to worry: many of the things you'll have to learn are not all that new, as you've already seen really similar things in Python.

    Some more JavaScript:
        - |
            In order to make sense of what comes next, a bit more knowledge about the JavaScript language will come in handy!
        -
            link: https://runestone.academy/runestone/books/published/JS4Python/Advanced/closures.html
            title: "More about Functions: Closures and Scopes"
        -
            link: https://javascript.info/arrow-functions-basics
            title: "Arrow functions, the basics"
        -
            link: https://www.youtube.com/watch?v=670f71LTWpM
            title: Asynchronous JavaScript in ~10 Minutes - Callbacks, Promises, and Async/Await
            info: "This is a great video explaining *callback hell* and it's solutions: Promises and async/await. You've seen similar constructs in Python before."

    Node.js & NPM:
        - |
            Use Manjaro's *Add/remove software* to install the `nodejs` and `npm` packages. 
        -
            link: https://www.youtube.com/watch?v=ENrzD9HAZK4
            title: Node.js Ultimate Beginner’s Guide in 7 Easy Steps
            info: This great video walks you through all of the basics of Node.js, NPM and Express. You can ignore the parts about installation and deploying to the Google cloud.
        -
            link: https://nodejs.dev/en/learn/differences-between-nodejs-and-the-browser/
            title: Differences between Node.js and the Browser
            info: A short read that points out the differences between programming browser JavaScript and Node.js JavaScript. Read attentively, if you're still somewhat fuzzy on this.
        -
            link: https://www.digitalocean.com/community/tutorials/how-to-use-node-js-modules-with-npm-and-package-json
            title: How To Use Node.js Modules with npm and package.json
            info: NPM is to Node.js as Poetry is to Python - both manage library dependencies. In fact, the developers of Poetry took lots of inspiration from NPM. So it should be pretty easy to get started with NPM. This link is a good reference if you need it, though.

    Stupid Simple Search Service:
        - |
            Our search engine is gonna be called the *Stupid Simple Search Service*. Some boilerplate code, including the HTML layout, has been provided. You can run it using:

            ```sh
            npm install && node search.js
            ```

            It should install all required NPM modules, and start listening on port 3456. You can open it in your browser through: http://127.0.0.1:3456 - In its current state, it is just showing fake data.

        -
            link: https://www.npmjs.com/package/express
            title: NPM repository - express
            info: Express is to Node.js as Flask is to Python - both are libraries for building web servers. Being the Flask ninja that you are, it shouldn't be hard to pick up. 

        -
            link: https://handlebarsjs.com/guide/#what-is-handlebars
            title: Handlebars - Introduction
            info: Handlebars is to Express as Jinja is to Flask - both are HTML templating engines. As in the assignment working HTML templates are provided, there's really little need to dive into the specifics of Handlebars at this point. But in case you need a readable reference, here it is.

        -
            link: https://github.com/express-handlebars/express-handlebars
            title: Express Handlebars - Documentation
            info: This document explains how Handlebars integrates into Express. This integration has already been done for you in the provided code, so there's probably little need to consult this.

    
    The database:
        -
            link: https://github.com/JoshuaWise/better-sqlite3/blob/master/docs/api.md
            title: better-sqlite3 - API documentation
            info: "better-sqlite3 is to Node.js as sqlite3 is to Python - both are libraries for using SQLite from your code. In fact, Node.js also has a package named just *sqlite3*, but better-sqlite3 is easier to use. Unfortunately, its getting started documentation is not stellar, but you'll figure it out! :-)"
        -
            ^merge: feature
            text: |
                Install the better-sqlite3 npm package (in such a way that it is also included in `packages.json`). Within `search.js` use it to open/create a database. Use the `exec` method to create a `pages` table (containing URLs, titles and texts) if the table doesn't exist yet (using `CREATE TABLE IF NOT EXISTS`).

    Add a site:
        -
            link: https://github.com/node-fetch/node-fetch/tree/2.x#readme
            title: NPM repository - node-fetch@2
            info: We'll use this NPM package to retrieve web pages in order to index them. There are many alternatives for doing HTTP requests, but this one has the advantage that is has the same API as the `fetch` function that is part of all modern web browser JavaScripts. So we'll be able to reuse this knowledge later on. We recommend that you use version 2 (instead of version 3) of this library.

        -
            ^merge: feature
            text: |
                Handle POSTs for the *Add a site* form. For now, we'll ignore the recursion depth, and index just the page for which the URL was submitted.

                Start by retrieving the body content for the page as text using the `node-fetch@2` package. Next use the provided `parseHtml` function to extract the page title and body text. Finally, insert the results into the `pages` table, and redirect the browser back to the main search page.

    Search!:
        -
            ^merge: feature
            text: |
                Implement the `get("/")` route to feed the Handlebars template with actual instead of fake data.

                - `page_count` should be the number of rows in the `pages` table.
                - `query` should just be the string that was received as the `terms` query parameter. It should be `undefined` if no query was given.
                - `results` should contain an array of objects, or should be undefined when no query was given. There should be one object for each page that contains the query string exactly. Each object should have an `url`, `title` and a `snippet` (which can just be the first 100 characters of the body text, for now).

    Recursively adding sites:
        -
            ^merge: feature
            text: |
                **Caution:** when creating programs like this, you should be really carefully not to cause inconvenience for others, by stressing the network or remote services. Always keep an eye on how many requests you are generating, and don't forget to stop your service immediately after a test.

                Although it is usually possible to [manually submit a new URL to a search engine](https://www.google.nl/intl/nl/add_url.html), search engines generally discover new pages automatically be following links it finds in other web pages, recursively. Let's build this!

                However, in order to be friendly on the network and on the sites we're indexing, we want to limit our indexer to working on just a single page at a time. Recursively discovered pages should be added to the end of a queue, for the indexer to pick up.

                Also, check whether a certain URL is already in your index before indexing it again.

                *Hint:* it's probably easiest to implement your indexer as an `async` function that iterates over the queue (removing the elements that is has worked on). Your program should take care to only start this function when it is not already running.

    Full text search:
        -
            link: https://sqlite.org/fts5.html#overview_of_fts5
            title: SQLite FTS5 Extension
            info: "The manual for the FTS5 full-text-search SQLite extension. Just read the first (overview) chapter and section 5.1.3 on the `snippet()` function."
        -
            ^merge: feature
            text: |
                And now for the bonus!

                Actual search engines apply *fuzzy search*, to also match search terms that are not an exact match or that have some words between them. This is actually pretty hard to do (well), and a lot of work. Fortunately, SQLite comes with a build-in full-text-search extension, FTS5. Use it to improve your search engine!

                *Hint:* you will need to recreate your `pages` table as a `virtual` table, as shown in the FTS5 manual.

                Secondly, try to use the `snippet()` SQLite function to have FTS5 attempt to return a relevant snippet (containing as many of the such terms as it can), instead of just the first 100 characters. You can also have it print the actual search terms in bold!

                Got all that? Awesome! Now you won't need Google anymore. :-)

