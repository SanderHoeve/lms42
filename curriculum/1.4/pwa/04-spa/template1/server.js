// Create an express ap
const express = require('express');
const app = express();

// Static data
app.use(express.static('public'));

// Automatically decode JSON bodies
app.use(express.json());

// Use the API subrouter
app.use('/api', require('./api'));

// Start accepting requests
const listener = app.listen(process.env.PORT, "0.0.0.0", function () {
  console.log('Your app is listening on port ' + listener.address().port);
});
