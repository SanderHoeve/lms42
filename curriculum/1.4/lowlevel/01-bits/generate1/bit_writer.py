class BitWriter:
    def __init__(self):
        self.bytes = bytearray()
        self.bit_offset = -1

    def write_bit(self, value):
        if self.bit_offset < 0:
            self.bit_offset = 7
            self.bytes.append(0)
        self.bytes[-1] |= (1 if value else 0) << self.bit_offset
        self.bit_offset -= 1

    def write_number(self, value, bit_count, signed=False):
        if signed and value < 0:
            value += 1<<bit_count

        assert(value >=0 and value < (1<<bit_count))

        for bit_num in range(bit_count-1, -1, -1):
            self.write_bit((value >> bit_num) & 1)
