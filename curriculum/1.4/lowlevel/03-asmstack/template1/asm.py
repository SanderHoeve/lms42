#!/bin/env python
import re
import sys
import os
import json
import subprocess
import codecs

PTR_SIZE = 2

with open(sys.argv[1], "r") as file:
    input_data = file.read()

tokenizer = re.compile('"(?:\\\\.|[^"])*"|\\*|\'(?:\\\\.|[^\'])*\'|#[^\n]*|\n|[^\\s]+', 0)
tokens = ['ip:', f'int{PTR_SIZE*8}', 'main', "\n"]
token_positions = [0, 0, 0, 0]
for m in tokenizer.finditer(input_data):
    tokens.append(m.group(0))
    token_positions.append(m.start())

token_pos = 0
def fetch_token(allow_eol=False):
    global token_pos
    token = tokens[token_pos]
    token_pos += 1
    if not allow_eol and (token[0]=="#" or token=="\n"):
        fail("Unexpected end of line")
    return token

def fetch_tokens_til(end, allow_eol=False):
    result = []
    while True:
        token = fetch_token(allow_eol)
        if token==end:
            return result
        result.append(token)

output = bytearray()

statement_offsets = [] # [(token_pos, output_pos), ..]

def show_log():
    for i in range(0, len(statement_offsets)-1):
        [token_pos1, output_pos1] = statement_offsets[i]
        [token_pos2, output_pos2] = statement_offsets[i+1]
        code = ' '.join(tokens[token_pos1:token_pos2]).strip()
        if len(code) and code[0] != '#':
            dump = ' '.join(map(lambda n : '{:02x}'.format(n), output[output_pos1:output_pos2]))
            print('{:05}'.format(output_pos1)+': '+code.ljust(32)+'  '+dump)

def fail(msg):
    show_log()
    line = input_data[0:token_positions[token_pos-1]].count('\n') + 1
    sys.exit(f"Error at line {line}: {msg}")

def fetch_argument():
    # Count the number of '*' indirections before the actual argument
    indirections = 0
    while True:
        arg = fetch_token()
        if arg != '*':
            return arg, indirections
        indirections += 1


labels = {} # {label: address, ..}
references = [] # [(reference_address, label), ...] Resolved based on `labels` at the end.
macros = {} # {name: (arg_names, body_tokens), ...}


instructions = {} # [opcode, argument count, check_flags_flags, size]
FIRST_REF = 1 # check_flags: the first argument must be a reference
ANY_REF = 2 # check_flags: any of the arguments must be a reference
ALLOW_RUN_THROUGH = 4 # check_flags: another statement may follow this one one the same line
for name, info in {
    "set": [1, 2, FIRST_REF],
    "add": [2, 2, FIRST_REF],
    "sub": [3, 2, FIRST_REF],
    "mul": [4, 2, FIRST_REF],
    "div": [5, 2, FIRST_REF],
    "and": [6, 2, FIRST_REF],
    "or": [7, 2, FIRST_REF],
    "xor": [8, 2, FIRST_REF],
    "if=": [9, 2, ANY_REF|ALLOW_RUN_THROUGH],
    "if>": [10, 2, ANY_REF|ALLOW_RUN_THROUGH],
    "if<": [11, 2, ANY_REF|ALLOW_RUN_THROUGH],
    "read": [12, 1, FIRST_REF],
    "write": [13, 1, 0],
    "int": [-1, 1, 0],
}.items():
    for bits in [8,16,32]:
        instructions[f"{name}{bits}"] = info + [bits//8]


while token_pos < len(tokens):
    # Remember the start token and the start address for each statement, used by show_log().
    statement_offsets.append([token_pos, len(output)])
    allow_run_through = True

    token = fetch_token(True)
    if token[0] == '#' or token=="\n": # a comment or an extra newline
        continue
    elif token[-1] == ':': # a label
        labels[token[0:-1]] = len(output)
        continue # no newline required
    elif token[0]=='"': # a data string
        string = bytes(token[1:-1],'utf-8').decode("unicode_escape")
        #string = json.loads(token)
        output.extend(map(ord, string))
    elif token in instructions:
        [opcode, arg_count, check_flags, size] = instructions[token]

        if (opcode>=0):
            output.append(opcode)
            arg_spec_pos = len(output)
            output.append(0)

        argSpec = {1: 0, 2: 1, 4: 2}[size]
        for arg_num in range(0, arg_count):
            arg, indirections = fetch_argument()

            if indirections>2 or (opcode<0 and indirections!=0):
                fail(f"Invalid reference depth for argument #{arg_num+1}")

            if len(arg)>=3 and arg[0]=="'" and arg[-1]=="'":
                # Argument is a character literal
                string = codecs.unicode_escape_decode(arg[1:-1])[0]
                number = ord(string)
            else:
                try:
                    # Is argument a number?
                    number = int(arg)
                except ValueError:
                    # Assume argument is a label reference
                    references.append((len(output), arg))
                    number = 0xffff
                    if indirections==0 and size != PTR_SIZE:
                        fail(f"Opcode size doesn't match pointer size")

            if indirections!=0 or number!=0 or opcode<0:
                argSpec = argSpec | ((indirections+1) << (2+arg_num*2))
                output.extend(number.to_bytes(length=(size if indirections==0 else PTR_SIZE), byteorder='little'))

        if check_flags & FIRST_REF and (argSpec & (1<<3))==0:
            fail(f"The first argument for {token} must dereference an address")

        if check_flags & ANY_REF and (argSpec & (1<<3))==0 and (argSpec & (1<<5))==0:
            fail(f"At least one argument for {token} must dereference an address")

        if check_flags & ALLOW_RUN_THROUGH:
            allow_run_through = True

        if opcode >= 0:
            output[arg_spec_pos] = argSpec
    elif token == 'macro': # a macro definition
        name = fetch_token()
        macros[name] = (fetch_tokens_til('{'), fetch_tokens_til('}', True))
    elif token in macros: # a macro call
        # Create a dict that maps argument names to their concrete values
        arg_map = {arg_name: fetch_argument() for arg_name in macros[token][0]}
        # Iterate the macro body tokens, replacing arguments with their concrete values 
        body = ["\n"]
        for token in macros[token][1]:
            if token in arg_map:
                token,indirections = arg_map[token]
                for _ in range(indirections):
                    body.append("*")
            body.append(token)
        # Insert the tokens after the macro invocation
        tokens[token_pos:token_pos] = body
        # We'll attribute all inserted tokens to the line number of the invocation (for error handling)
        token_positions[token_pos:token_pos] = [token_positions[token_pos-1]] * len(body)
    else:
        fail(f"Unknown opcode `{token}`")

    if not allow_run_through and token_pos < len(tokens):
        token = fetch_token(True)
        if token[0]!='#' and token!="\n":
            fail(f"Newline expected after statement but found `{token}`")

statement_offsets.append([token_pos, len(output)])

# Resolve all address references
for pos,label in references:
    if not label in labels:
        fail(f"No such label: '{label}'")
    output[pos:pos+PTR_SIZE] = labels[label].to_bytes(length=PTR_SIZE, byteorder="little")

show_log()

with open(os.path.splitext(sys.argv[1])[0] + '.executable', 'wb') as file:
    file.write(output)
